package smartcity.loborobot.com.smartcityapp.callable;

import android.app.Activity;
import android.support.v4.app.FragmentManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import smartcity.loborobot.com.smartcityapp.MainActivity;
import smartcity.loborobot.com.smartcityapp.PlaceholderHistoryFragment;
import smartcity.loborobot.com.smartcityapp.R;
import smartcity.loborobot.com.smartcityapp.extra.MainPrivateActivity;
import smartcity.loborobot.com.smartcityapp.extra.PlaceholderHistoryPrivateFragment;
import smartcity.loborobot.com.smartcityapp.info.HistoryInfo;
import smartcity.loborobot.com.smartcityapp.json.JSONDecoderAll;
import smartcity.loborobot.com.smartcityapp.message.Message;


/**
 * Created by fincyt on 13/01/15.
 */

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */
public class PublicDeviceSensorsProcedure implements Procedure{
    /**
     * data in text to decode
     * */
    private Activity main;
    public void setActivity(Activity a){
        main=a;
    }
    public Activity getActivity(){
        return main;
    }
    private String data;
    public void setData(String d){
        data=d;
    }
    public String getData(){
        return data;
    }
    private List<String> fields=new ArrayList();
    public void setFields(List<String> fs){
        for(String s:fs){
            fields.add(s);
        }
    }
    public List<String> getFields(){
        return fields;
    }
    private List objects=new ArrayList();
    public void setObjects(List os){
        for(Object o:os){
            objects.add(o);
        }
    }
    public List getObjects(){
        return objects;
    }

    public static class HistoryInfoContainer{
        private static HistoryInfoContainer singleton;
        private static List<HistoryInfo> data;
        public static HistoryInfoContainer getSingleton(){
            if(singleton==null){
                singleton=new HistoryInfoContainer();
            }
            return singleton;
        }
        private HistoryInfoContainer(){
            data=new ArrayList();
        }
        public static void addHistory(HistoryInfo h){
            data.add(h);
        }
        public static void setData(List<HistoryInfo> lista){
            data=new ArrayList();
            for(HistoryInfo d:lista){
                addHistory(d);
            }
        }
        public static List<HistoryInfo> getData(){
            return data;
        }
        public static HistoryInfo getHistory(int index){
            return data.get(index);
        }
    }
    public void execute(){
        String dataString=getData();

        Message.getSingleton().setContext(getActivity().getApplicationContext());

        ///Message.getSingleton().show(dataString);

        List<HashMap> data=new ArrayList();
        try{
            data= JSONDecoderAll.decode(dataString, getFields());
        }catch(Exception ex){}

        List<HistoryInfo> list_histories=new ArrayList();
        for(HashMap d:data) {
            HistoryInfo object = new HistoryInfo();
            object.setData(d);
            list_histories.add(object);
        }

        HistoryInfoContainer.getSingleton().setData(list_histories);

        //Message.getSingleton().show(list_histories.toString());

        FragmentManager fragmentManager = null;
        try{
            fragmentManager= MainActivity.FragmentManagerContainer.getSingleton().getFragmentManager();
        }catch(Exception  ex){}
        if(fragmentManager!=null){
            PlaceholderHistoryFragment fragment= PlaceholderHistoryFragment.newInstance(0);
            fragment.setMainActivity(  MainActivity.ActivityContainer.getSingleton().getMainActivity() );
            fragmentManager.beginTransaction().replace(R.id.container,fragment ).commit();
        }
    }
}

