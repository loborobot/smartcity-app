package smartcity.loborobot.com.smartcityapp.json;

import java.util.*;

import org.json.*;

import smartcity.loborobot.com.smartcityapp.MainActivity;
import smartcity.loborobot.com.smartcityapp.message.Message;

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */

public class JSONDecoderAll {

	public static List<HashMap> decode(String dataString,List< String > fields){
		List<HashMap> data=new ArrayList();
		JSONArray object=null;
		try{
		 	object=new JSONArray(dataString);
		}catch(Exception ex){}
		if(object!=null){
			for(int i=0;i<object.length();i++){			
				JSONObject o=null;
				HashMap h=new HashMap();
				try{
					o=object.getJSONObject(i);					
					String val;
					for(String f : fields){
						val="";
						try{
							val=o.get(f).toString();
						}catch(Exception ex){
							val="";
						}
						if(val!=""){							
							h.put(f,val);							
						}

					}
					data.add(h);
		 		}catch(Exception ex){}
		 	}	
		}

		return data;
	}

}