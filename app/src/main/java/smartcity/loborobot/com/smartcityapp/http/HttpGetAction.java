package smartcity.loborobot.com.smartcityapp.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.*;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import java.util.*;
import java.util.logging.Logger;

import smartcity.loborobot.com.smartcityapp.MainActivity;
import smartcity.loborobot.com.smartcityapp.message.Message;

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */

public class HttpGetAction{

	public static String send(List<HashMap> data,String url){
		HttpClient httpClient = new DefaultHttpClient();
		String params = "";
		for(HashMap d: data){
            try{
                params=params+d.get("field").toString()+"="+d.get("data").toString()+"&";
            }catch(Exception ex){}
		}
        try{
            url=url+"&"+params;
        }catch(Exception ex){}

        //Message.getSingleton().setContext(MainActivity.ActivityContainer.getSingleton().getActivity());
        //Message.getSingleton().show(url);

        Logger log= Logger.getLogger("debug");
        log.info("url: "+url);

		HttpGet http = new HttpGet(url);
		try {
			try {
				HttpResponse httpResponse = httpClient.execute(http);
				InputStream inputStream = httpResponse.getEntity().getContent();
				InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
				BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
				StringBuilder stringBuilder = new StringBuilder();
				String bufferedStrChunk = null;
				while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
					stringBuilder.append(bufferedStrChunk);
				}
				return stringBuilder.toString();
			} catch (ClientProtocolException cpe) {
//				//System.out.println("First Exception of HttpResponse :"+ cpe);
				cpe.printStackTrace();
			} catch (IOException ioe) {
//				//System.out.println("Second Exception of HttpResponse :"+ ioe);
				ioe.printStackTrace();
			}
		} catch (Exception uee) {
			//System.out.println("An Exception given because of UrlEncodedFormEntity argument :"+ uee);
			uee.printStackTrace();
		}
		return "";
	}
}
