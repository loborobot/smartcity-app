package smartcity.loborobot.com.smartcityapp.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.*;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.entity.*;

import org.json.JSONObject;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import org.json.JSONStringer;

import java.util.*;

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */

public class HttpPostJsonAction{
	public static String send(List<HashMap> data,String url){
		HttpClient httpClient = new DefaultHttpClient();
		HttpPost http = new HttpPost(url);
		String json = "";
                JSONObject jsonObject = new JSONObject();
		for(HashMap d: data){	
			try{		
		            jsonObject.accumulate(d.get("field").toString(), d.get("data").toString());
			}catch(Exception ex){}
		}
	        json = jsonObject.toString();
                StringEntity se = null;
		try{
			se=new StringEntity(json);
		}catch(Exception ex){}
		try {
			if(se!=null){
				http.setEntity(se);
			}

			http.setHeader("Accept", "application/json");
		        http.setHeader("Content-type", "application/json");
			try {
				HttpResponse httpResponse = httpClient.execute(http);
				InputStream inputStream = httpResponse.getEntity().getContent();
				InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
				BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
				StringBuilder stringBuilder = new StringBuilder();
				String bufferedStrChunk = null;
				while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
					stringBuilder.append(bufferedStrChunk);
				}
				return stringBuilder.toString();
			} catch (ClientProtocolException cpe) {
				System.out.println("First Exception of HttpResponse :"+ cpe);
				cpe.printStackTrace();
			} catch (IOException ioe) {
				System.out.println("Second Exception of HttpResponse :"+ ioe);
				ioe.printStackTrace();
			}
		} catch (Exception uee) {
			System.out.println("An Exception given because of UrlEncodedFormEntity argument :"+ uee);
			uee.printStackTrace();
		}
		return null;
	}
}
