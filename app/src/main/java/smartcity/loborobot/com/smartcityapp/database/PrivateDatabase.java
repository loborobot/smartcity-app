package smartcity.loborobot.com.smartcityapp.database;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.HashMap;

import smartcity.loborobot.com.smartcityapp.MainActivity;
import smartcity.loborobot.com.smartcityapp.extra.MainPrivateActivity;

/**
 * Created by fincyt on 16/01/15.
 */

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */


 public class PrivateDatabase extends Database { //access database from MainPrivateActivity

    private static PrivateDatabase singleton;

    public static PrivateDatabase getSingleton(){
        if(singleton==null){
            singleton=new PrivateDatabase();
        }
        return singleton;
    }

    public PrivateDatabase(){
        super();
    }

    public static SQLiteDatabase getDB(){
        return MainPrivateActivity.BDContainer.getSingleton().getDb();
    }

    public static HashMap getUser(){
        Cursor resultSet = getDB().rawQuery("Select * from user where id=1",null);
        resultSet.moveToFirst();
        String username = resultSet.getString(1);
        String password = resultSet.getString(2);
        String success= resultSet.getString(3);
        String token=resultSet.getString(4);
        HashMap data=new HashMap();
        data.put("username",username);
        data.put("password",password);
        data.put("success",success);
        data.put("token",token);
        return data;
    }


}
