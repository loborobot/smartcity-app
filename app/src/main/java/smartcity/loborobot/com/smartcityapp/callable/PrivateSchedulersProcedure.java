package smartcity.loborobot.com.smartcityapp.callable;

import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import smartcity.loborobot.com.smartcityapp.R;
import smartcity.loborobot.com.smartcityapp.extra.MainPrivateActivity;
import smartcity.loborobot.com.smartcityapp.extra.data.SchedulerAdapter;
import smartcity.loborobot.com.smartcityapp.extra.data.SchedulerData;
import smartcity.loborobot.com.smartcityapp.info.DeviceInfo;
import smartcity.loborobot.com.smartcityapp.info.SchedulerInfo;
import smartcity.loborobot.com.smartcityapp.json.JSONDecoderAll;
import smartcity.loborobot.com.smartcityapp.message.Message;

/**
 * Created by fincyt on 13/01/15.
 */

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */
public class PrivateSchedulersProcedure implements Procedure{
    /**
     * data in text to decode
     * */
    private Activity main;
    public void setActivity(Activity a){
        main=a;
    }
    public Activity getActivity(){
        return main;
    }
    private String data;
    public void setData(String d){
        data=d;
    }
    public String getData(){
        return data;
    }
    private List<String> fields=new ArrayList();
    public void setFields(List<String> fs){
        for(String s:fs){
            fields.add(s);
        }
    }
    public List<String> getFields(){
        return fields;
    }
    private List objects=new ArrayList();
    public void setObjects(List os){
        for(Object o:os){
            objects.add(o);
        }
    }
    public List getObjects(){
        return objects;
    }
    public void execute(){

        Message.getSingleton().setContext(getActivity().getApplicationContext());

        String dataString=getData();

        List<SchedulerData> sdata=new ArrayList();

        List<HashMap> data=new ArrayList();
        try{
            data= JSONDecoderAll.decode(dataString, getFields());
        }catch(Exception ex){}


        //Message.getSingleton().show(data.toString());

        for(HashMap d:data){
            SchedulerInfo object=new SchedulerInfo();
            object.setData(d);

            String title="";
            try{
                title=object.getName();
                title=title.trim();
            }catch(Exception ex){}
            String hour="";
            try{
                hour=object.getTimeBegin();
                hour=hour.trim();
            }catch(Exception ex){}

            String enabled="";
            try{
                enabled=object.getEnabled();
            }catch(Exception ex){}

            Boolean flag=false;
            try{
                flag=Boolean.parseBoolean(enabled);
            }catch(Exception ex){}

            SchedulerData sd=new SchedulerData(title,flag,hour);
            sdata.add(sd);

            //Message.getSingleton().show(title.toString());

            objects.add(object);
        }

        SchedulerData scheduler_data[]=new SchedulerData[sdata.size()];
        int cont=0;
        for(SchedulerData dt:sdata){
            scheduler_data[cont]=dt;
            cont++;
        }

        //Message.getSingleton().show(scheduler_data.toString());

        SchedulerAdapter adapter=null;
        try {
            adapter = new SchedulerAdapter(getActivity().getApplicationContext(), R.layout.extra_schedulers_listview_item_row, scheduler_data);
            if(adapter!=null){
                adapter.setActivity(MainPrivateActivity.ActivityContainer.getSingleton().getActivity());
            }

        }catch(Exception ex){}

        try{
            MainPrivateActivity.PlaceholderPrivateFragment.ListViewContainer.getSingleton().getListView().setAdapter(adapter);
        }catch(Exception ex){}/**/

        /*try{
            MainPrivateActivity.PlaceholderPrivateFragment.ListViewContainer.getSingleton().getListView().setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Message.getSingleton().setContext(MainPrivateActivity.ActivityContainer.getSingleton().getActivity().getApplicationContext());
                            Message.getSingleton().show("Hola ");
                        }

                    }
            );
        }catch(Exception ex){}/**/

        try{
            MainPrivateActivity.PlaceholderPrivateFragment.ListViewContainer.getSingleton().getListView().setOnItemClickListener(
                    new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            Message.getSingleton().setContext(MainPrivateActivity.ActivityContainer.getSingleton().getActivity().getApplicationContext());
//                            Message.getSingleton().show("Hola item");
                        }
                    }
            );
        }catch(Exception ex){}/**/
    }
}