package smartcity.loborobot.com.smartcityapp.extra.data;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import smartcity.loborobot.com.smartcityapp.R;

import smartcity.loborobot.com.smartcityapp.extra.MainPrivateActivity;
import smartcity.loborobot.com.smartcityapp.extra.SchedulersActivity;

/**
 * Created by Christian Portilla Pauca on 20/12/14.
 */

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */

 public class SchedulerAdapter extends ArrayAdapter<SchedulerData> {

    private Context context;
    private int layoutResourceId;
    private SchedulerData data[] = null;

    private MainPrivateActivity activity;

    private Activity main;

    public void setMainPrivateActivity(MainPrivateActivity a){
        activity=a;
    }
    public MainPrivateActivity getMainPrivateActivity(){
        return activity;
    }

    public SchedulerAdapter(Context context, int layoutResourceId, SchedulerData[] data) {
        super(context,layoutResourceId,data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        SchedulerHolder holder = null;
        if(context==null){
            context=getActivity().getApplicationContext();
        }
        if(row == null)
        {
            try {
                row = SchedulersActivity.ObjectSingleton.getSingleton().getSchedulersActivity().getLayoutInflater().inflate(layoutResourceId, parent, false);
            }catch(Exception ex){}

            holder = new SchedulerHolder();
            try {
                if(row!=null) {
                    holder.switchScheduler = (Switch) row.findViewById(R.id.switchScheduler);
                }
            }catch(Exception ex){}
            try {
                if(row!=null) {
                    holder.txtTitle = (TextView) row.findViewById(R.id.txtTitle);
                }
            }catch (Exception ex){}
            try {
                if(row!=null) {
                    holder.txtHour = (TextView) row.findViewById(R.id.txtHour);
                }
            }catch (Exception ex){}
            try {
                if(row!=null) {
                    row.setTag(holder);
                }
            }catch(Exception ex){}
        }
        else
        {
            holder = (SchedulerHolder)row.getTag();
        }

        SchedulerData scheduler= data[position];
        holder.txtTitle.setText(scheduler.title);
        holder.switchScheduler.setChecked(scheduler.enable);
        holder.txtHour.setText(scheduler.hour);

        return row;
    }

    public void setActivity(Activity activity) {
        main = activity;
    }

    public Activity getActivity(){
        return main;
    }

    public static class SchedulerHolder
    {

        public TextView     txtTitle;
        public Switch       switchScheduler;
        public TextView     txtHour;
    }
}