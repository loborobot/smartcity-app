package smartcity.loborobot.com.smartcityapp;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.SupportMapFragment;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;

import java.util.ArrayList;
import java.util.List;

import smartcity.loborobot.com.smartcityapp.callable.DevicesProcedure;
import smartcity.loborobot.com.smartcityapp.callable.PrivateDeviceSensorsProcedure;
import smartcity.loborobot.com.smartcityapp.callable.Procedure;
import smartcity.loborobot.com.smartcityapp.callable.PublicDeviceSensorsProcedure;
import smartcity.loborobot.com.smartcityapp.callable.UserLoginProcedure;
import smartcity.loborobot.com.smartcityapp.database.Database;
import smartcity.loborobot.com.smartcityapp.extra.*;
import smartcity.loborobot.com.smartcityapp.extra.maps.MarkerInfo;
import smartcity.loborobot.com.smartcityapp.info.*;
import smartcity.loborobot.com.smartcityapp.message.Message;
import smartcity.loborobot.com.smartcityapp.model.*;
import smartcity.loborobot.com.smartcityapp.session.UserSession;

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */
public class MainActivity extends android.support.v7.app.ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    public static class FragmentManagerContainer{
        private static FragmentManager fragmentManager;
        private static FragmentManagerContainer singleton;
        private FragmentManagerContainer(){

        }
        public static void setFragmentManager(FragmentManager manager){
            fragmentManager=manager;
        }
        public static FragmentManager getFragmentManager(){
            return fragmentManager;
        }
        public static FragmentManagerContainer getSingleton() {
            if(singleton==null){
                singleton=new FragmentManagerContainer();
            }
            return singleton;
        }
    }

    public static class ActivityContainer{
        private static ActivityContainer singleton;
        private static Activity activity;
        private static MainActivity main;
        public void setMainActivity(MainActivity a){
            main=a;
        }
        public MainActivity getMainActivity(){
            return main;
        }

        public void setActivity(Activity a){
            activity=a;
        }
        public Activity getActivity(){
            return activity;
        }
        private ActivityContainer(){

        }
        public static ActivityContainer getSingleton(){
            if(singleton==null){
                singleton=new ActivityContainer();
            }
            return singleton;
        }
    }
    /*
    * BDContainer for Main Activity
     */
    public static class BDContainer{
        private static SQLiteDatabase db= null;
        private static BDContainer singleton;
        private BDContainer(){

        }
        public static  void setDb(SQLiteDatabase database){
            db=database;
        }
        public static SQLiteDatabase getDb(){
            return db;
        }
        public static BDContainer getSingleton(){
            if(singleton==null){
                singleton=new BDContainer();
            }
            return singleton;
        }
    }

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    /*
    * Left show data in view
    *
    * */

    public void leftShowHistory(View v){

        Message.getSingleton().setContext(MainActivity.ActivityContainer.getSingleton().getActivity().getApplicationContext());
        //Message.getSingleton().show("left");

        int index = PlaceholderHistoryFragment.SensorIndex.getSingleton().getIndex();
        index--;
        if(index<0){index=6;}

        List<HistoryInfo> list_histories =null;

        //Message.getSingleton().show(" : "+index);

        try{
            list_histories= PublicDeviceSensorsProcedure.HistoryInfoContainer.getSingleton().getData();
        }catch(Exception ex){}
        HistoryInfo data=null;
        try{
            data=list_histories.get(index);
        }catch(Exception ex){}

        //Message.getSingleton().show("size: "+list_histories.size());

        String sensors[]={"temperature","humidity","sound","flowmeter","light","nitrogen_dioxide","carbon_monoxide"};
        String measures[]={"C°","%RH","DB","L/H","LUX","PBM","PPM"};
        if(data!=null) {
            TextView texto = null;
            try{
                texto=(TextView) findViewById(R.id.textViewHistoryPublic);
            }catch(Exception ex){}

            String label="";
            String measure="";
            String value="";
            try{
                label=sensors[index].toUpperCase();
            }catch(Exception ex){
                label="";
            }
            try{
                measure=measures[index].toUpperCase();
            }catch(Exception ex){
                measure="";
            }
            try{
                value=data.getValue();
            }catch(Exception ex){
                value="";
            }
            //Message.getSingleton().show("string: "+label);
            try{
                texto.setText(label+" : "+value+" "+measure);
            }catch(Exception ex){}

        }
        PlaceholderHistoryFragment.SensorIndex.getSingleton().setIndex(index);

    }

    /*
    * Right show data in view
    *
    * */

    public void rightShowHistory(View v){
        Message.getSingleton().setContext(MainActivity.ActivityContainer.getSingleton().getActivity().getApplicationContext());
        //Message.getSingleton().show("right");

        int index = PlaceholderHistoryFragment.SensorIndex.getSingleton().getIndex();
        index++;
        if(index>6){index=0;}
        List<HistoryInfo> list_histories =null;

        //Message.getSingleton().show(" : "+index);

        try{
            list_histories= PublicDeviceSensorsProcedure.HistoryInfoContainer.getSingleton().getData();
        }catch(Exception ex){}
        HistoryInfo data=null;
        try{
            data=list_histories.get(index);
        }catch(Exception ex){}

        //Message.getSingleton().show("size: "+list_histories.size());

        String sensors[]={"temperature","humidity","sound","flowmeter","light","nitrogen_dioxide","carbon_monoxide"};
        String measures[]={"C°","%RH","DB","L/H","LUX","PBM","PPM"};
        if(data!=null) {
            TextView texto = null;
            try{
                texto=(TextView) findViewById(R.id.textViewHistoryPublic);
            }catch(Exception ex){}

            String label="";
            String measure="";
            String value="";
            try{
                label=sensors[index].toUpperCase();
            }catch(Exception ex){
                label="";
            }
            try{
                measure=measures[index].toUpperCase();
            }catch(Exception ex){
                measure="";
            }
            try{
                value=data.getValue();
            }catch(Exception ex){
                value="";
            }
            //Message.getSingleton().show("string: "+label);
            try{
                texto.setText(label+" : "+value+" "+measure);
            }catch(Exception ex){}

        }
        PlaceholderHistoryFragment.SensorIndex.getSingleton().setIndex(index);
    }

    private void startActivityAfterCleanup(Class<?> cls) {
        Intent intent = null;
        try{
            intent=new Intent(MainActivity.this, cls);
        }catch(Exception ex){}
        try {
            if(intent!=null) {
                startActivity(intent);
            }
        }catch(Exception ex){}
    }

    /*
    * click login
    * */
    public void onClickLogin(View v){
        String user="";
        String password="";

        TextView userText=(TextView)findViewById(R.id.username);
        TextView passText=(TextView)findViewById(R.id.password);

        user=userText.getText().toString();
        password=passText.getText().toString();

        UserSession.getSingleton().setUsername(user);
        UserSession.getSingleton().setPassword(password);

        //Message.getSingleton().setContext(ActivityContainer.getSingleton().getActivity());
        //Message.getSingleton().show("data: "+user+" : "+password);

        //
        // Database.getSingleton().execute();



        

        Procedure procedure=new UserLoginProcedure();
        UserModel model=new UserModel();
        model.setProcedure(procedure);
        model.setActivity(ActivityContainer.getSingleton().getActivity());
        model.Login(user, password);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        ActivityContainer.getSingleton().setActivity(this);

        SQLiteDatabase db=null;
        try{
            db=openOrCreateDatabase("SmartCityDB.db",Context.MODE_PRIVATE,null);
        }catch(Exception ex){}
        BDContainer.getSingleton().setDb(db);

        Database.getSingleton().createTableUser();
        Database.getSingleton().insertUserDummy();

    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();

        FragmentManagerContainer.getSingleton().setFragmentManager(fragmentManager);
        PlaceholderFragment fragment=PlaceholderFragment.newInstance(position+1);
        fragmentManager.beginTransaction()
                .replace(R.id.container, fragment )
                .commit();

        fragment.setMainActivity(this);
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(
                                R.string.title_sectionLogin
                         );
                SingletonMenu.getSingletonMenu().setOption(1);

                break;
            case 2:
                mTitle = getString(R.string.title_sectionDevices);
                SingletonMenu.getSingletonMenu().setOption(2);
                break;
            default:
                mTitle = getString(R.string.title_sectionDevices);
                SingletonMenu.getSingletonMenu().setOption(2);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public static class MarkersContainer{
            private static List<MarkerInfo> markers=null;
            private static MarkersContainer singleton=null;

            public static MarkersContainer getSingleton(){
                if(singleton==null){
                    singleton=new MarkersContainer();
                }
                return singleton;
            }
            private MarkersContainer(){
                markers=new ArrayList();
            }
            public void add(MarkerInfo m){
                markers.add(m);
            }
            public void setMarkers(List<MarkerInfo> lm){
                for(MarkerInfo m:lm){
                    markers.add(m);
                }
            }
            public MarkerInfo getMarker(int index){
                return (MarkerInfo) getMarkers().get(index);
            }
            public List<MarkerInfo> getMarkers(){
                return markers;
            }
            //*
            // Searching marker info taking id of marker info
            // This gets MarkerInfo taking into account id of device
            // *//
            public MarkerInfo searchMarkerById(String id){
                MarkerInfo result=null;
                for(MarkerInfo m:getMarkers()){
                    if(m.getId().compareTo(id)==0){
                        result=m;
                    }
                }
                return result;
            }
            //*
            // Searching marker info taking id of marker
            // This gets an object marker by its id
            // *//
            public MarkerInfo searchByMarkerId(String id){
                MarkerInfo result=null;
                for(MarkerInfo m:getMarkers()){
                    if(m.getMarker().getId().compareTo(id)==0){
                        result=m;
                    }
                }
                return result;
            }
        }

        public static class MapContainer{
            private static MapContainer singleton;
            private static GoogleMap mGoogleMap;
            private SupportMapFragment supportMapFragment=null;
            public void setSupportMapFragment(SupportMapFragment smf){
                supportMapFragment=smf;
                try{
                    mGoogleMap = getSupportMapFragment().getMap();
                }catch(Exception ex){}
            }
            public SupportMapFragment getSupportMapFragment(){
                return supportMapFragment;
            }
            public GoogleMap getMap(){
                return mGoogleMap;
            }
            private MapContainer(){

            }
            public static MapContainer getSingleton(){
                if(singleton==null){
                    singleton=new MapContainer();
                }
                return singleton;
            }

        }

        private static GoogleMap mGoogleMap;

        private SupportMapFragment supportMapFragment=null;

        public void setSupportMapFragment(SupportMapFragment smf){
            supportMapFragment=smf;
        }

        private LatLng AQP;


        private LayoutInflater inflater;
        public void setLayoutInflater(LayoutInflater l){
            inflater=l;
        }
        public LayoutInflater getLayoutInflater(){
            return inflater;
        }
        private ViewGroup container;
        public void setViewGroup(ViewGroup v){
            container=v;
        }
        public ViewGroup getViewGroup(){
            return container;
        }
        private Bundle savedInstanceState;

        public void setBundle(Bundle b){
            savedInstanceState=b;
        }
        public Bundle getBundle(){
            return savedInstanceState;
        }


        private static MainActivity activity;

        public static void setMainActivity(MainActivity a){
            activity=a;
        }
        public static MainActivity getMainActivity(){
            return activity;
        }

        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
            AQP=new LatLng(-16.3992433, -71.5369622);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            setLayoutInflater(inflater);
            setViewGroup(container);
            setBundle(savedInstanceState);

            View rootView =null;
            int opcion=SingletonMenu.getSingletonMenu().getOption();
            if(opcion==1){
                rootView=onCreateLoginView(inflater,container,savedInstanceState);
            }
            if(opcion==2){
                rootView=onCreateMainView(inflater,container,savedInstanceState);
            }
            return rootView;
        }



        public View onCreateMainView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);

            FragmentActivity activity=getActivity();
            FragmentManager manager=activity.getSupportFragmentManager();

            List<Fragment> fragments = new ArrayList();
            try {
                fragments = manager.getFragments();
            }catch(Exception ex){}

            FragmentManager fr=null;
            SupportMapFragment smf=null;
            for(Fragment f:fragments){
                try{fr=f.getChildFragmentManager();}catch(Exception ex){}
                try{smf=(SupportMapFragment) fr.findFragmentById(R.id.mapPublic);}catch(Exception ex){smf=null;}
                if(smf!=null){
                    try{
                        setSupportMapFragment(smf);
                        MapContainer.getSingleton().setSupportMapFragment(smf);
                    }catch(Exception ex){}
                    try{
                        //mGoogleMap = supportMapFragment.getMap();
                    }catch(Exception ex){}
                    if(MapContainer.getSingleton().getMap()!=null) {
                        try {
                            MapContainer.getSingleton().getMap().setMyLocationEnabled(true);
                        } catch (Exception ex) {
                        }
                        try {
////                            MapContainer.getSingleton().getMap().addMarker(new MarkerOptions().position(AQP).title("Arequipa"));
                        } catch (Exception ex) {
                        }
                        try {
                            MapContainer.getSingleton().getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(AQP, 14));
                        } catch (Exception ex) {
                        }
                        try {
                            MapContainer.getSingleton().getMap().animateCamera(CameraUpdateFactory.zoomBy(18));
                        } catch (Exception ex) {
                        }
                        try {
                            MapContainer.getSingleton().getMap().moveCamera(CameraUpdateFactory.newLatLng(AQP));
                        } catch (Exception ex) {
                        }
                        try{
                            MapContainer.getSingleton().getMap().setMapType(GoogleMap.MAP_TYPE_HYBRID);
                        }catch(Exception ex){}

                        Procedure procedure=new DevicesProcedure();
                        DeviceModel model=new DeviceModel();
                        model.setProcedure(procedure);
                        model.setActivity(getActivity());
                        model.getAll();
                    }
                }
            }

            return rootView;
        }

        private static GoogleMap getMap() {
            return mGoogleMap;
        }

        public View onCreateLoginView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_login, container, false);
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }

}
