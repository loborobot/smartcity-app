package smartcity.loborobot.com.smartcityapp.callable;

import android.app.Activity;
import android.content.Intent;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import smartcity.loborobot.com.smartcityapp.MainActivity;
import smartcity.loborobot.com.smartcityapp.database.Database;
import smartcity.loborobot.com.smartcityapp.extra.MainPrivateActivity;
import smartcity.loborobot.com.smartcityapp.info.DeviceInfo;
import smartcity.loborobot.com.smartcityapp.json.JSONDecoder;
import smartcity.loborobot.com.smartcityapp.json.JSONDecoderAll;
import smartcity.loborobot.com.smartcityapp.message.Message;
import smartcity.loborobot.com.smartcityapp.session.UserSession;

/**
 * Created by fincyt on 13/01/15.
 */

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */
public class UserLoginProcedure implements Procedure{
    /**
     * data in text to decode
     * */
    public UserLoginProcedure(){
        List<String> fieldAs=new ArrayList();
        fieldAs.add("success");
        fieldAs.add("token");
        setAnswerFields(fieldAs);
    }
    private Activity main;
    public void setActivity(Activity a){
        main=a;
    }
    public Activity getActivity(){
        return main;
    }
    private String data;
    public void setData(String d){
        data=d;
    }
    public String getData(){
        return data;
    }
    private List<String> fields=new ArrayList();
    private List<String> answerFields=new ArrayList();
    public void setAnswerFields(List<String> fs){
        for(String s:fs){
            answerFields.add(s);
        }
    }
    public List<String> getAnswerFields(){
        return answerFields;
    }
    public void setFields(List<String> fs){
        for(String s:fs){
            fields.add(s);
        }
    }
    public List<String> getFields(){
        return fields;
    }
    private List objects=new ArrayList();
    public void setObjects(List os){
        for(Object o:os){
            objects.add(o);
        }
    }
    public List getObjects(){
        return objects;
    }
    public void execute() {


        Message.getSingleton().setContext(MainActivity.ActivityContainer.getSingleton().getActivity().getApplicationContext());

        ///Message.getSingleton().show("Login ....");

        String dataString = getData();
        HashMap data = new HashMap();
        try {
            data = JSONDecoder.decode(
                    dataString,
                    getAnswerFields()
            );
        } catch (Exception ex) {
        }

        if(data.get("success").toString().compareTo("true")==0){

            try{
                UserSession.getSingleton().setToken(data.get("token").toString());
            }catch(Exception ex){}
            try{
                UserSession.getSingleton().setSuccess(data.get("success").toString());
            }catch(Exception ex){}
            try{
                Database.getSingleton().updateUser(UserSession.getUsername(),UserSession.getPassword(),UserSession.getSuccess(),UserSession.getToken());
            }catch(Exception ex){}

            Intent intent = null;
            try{
                intent=new Intent(MainActivity.ActivityContainer.getSingleton().getActivity().getApplicationContext(), MainPrivateActivity.class);
            }catch(Exception ex){}
            try {
                if(intent!=null) {
                    MainActivity.ActivityContainer.getSingleton().getActivity().startActivity(intent);
                }
            }catch(Exception ex){}


        }else{
            Message.getSingleton().show("Su usuario o contraseña no son aceptados por el sistema, por favor intente otra vez");
        }


    }
}
