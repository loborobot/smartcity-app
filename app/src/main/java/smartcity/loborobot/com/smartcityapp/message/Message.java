package smartcity.loborobot.com.smartcityapp.message;

import android.widget.*;
import android.app.*;
import android.content.*;
import android.os.*;

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */

public class Message {
    private Message(){

    }
    private static Message instance;
    public  static Message getSingleton(){
        if(instance==null){
            instance=new Message();
        }
        return instance;
    }
	private static Context context;
	private static CharSequence message;
	public static void setContext(Context c){
		context=c;
	}
	public static void show(String message){
		CharSequence text = message;
		int duration = Toast.LENGTH_LONG;
		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
	}
}