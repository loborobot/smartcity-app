package smartcity.loborobot.com.smartcityapp.extra.schedulers;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import java.util.logging.Logger;

import smartcity.loborobot.com.smartcityapp.R;
import smartcity.loborobot.com.smartcityapp.callable.PrivateActivitySchedulersProcedure;
import smartcity.loborobot.com.smartcityapp.callable.PrivateUpdateSchedulersProcedure;
import smartcity.loborobot.com.smartcityapp.callable.Procedure;
import smartcity.loborobot.com.smartcityapp.extra.SchedulersActivity;
import smartcity.loborobot.com.smartcityapp.message.Message;
import smartcity.loborobot.com.smartcityapp.model.SchedulerModel;
import smartcity.loborobot.com.smartcityapp.session.UserSession;

/**
 * Created by guest on 02/04/15.
 */
public class ArrayAdapterItem extends ArrayAdapter<ObjectItem> {
    Context mContext;
    int layoutResourceId;
    ObjectItem data[] = null;

    public ArrayAdapterItem(Context mContext, int layoutResourceId, ObjectItem[] data) {

        super(mContext, layoutResourceId, data);

        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //Message.getSingleton().setContext(mContext);
        //Message.getSingleton().show("hola");

        if(convertView==null){
            // inflate the layout
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);
        }

        // object item based on the position
        ObjectItem objectItem = data[position];

        // get the TextView and then set the text (item name) and tag (item ID) values
        TextView textViewItem = (TextView) convertView.findViewById(R.id.txtTitle);
        textViewItem.setText(objectItem.getValue("name"));
        textViewItem.setTag(objectItem.getValue("id"));

        TextView textHourViewItem = (TextView) convertView.findViewById(R.id.txtHour);
        textHourViewItem.setText(objectItem.getValue("hour"));
        textHourViewItem.setTag(objectItem.getValue("id"));

        Switch switchScheduler = (Switch) convertView.findViewById(R.id.switchScheduler);
        switchScheduler.setChecked(Boolean.parseBoolean(objectItem.getValue("enable")));
        switchScheduler.setTag(objectItem.getValue("id"));

        switchScheduler.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Message.getSingleton().setContext(buttonView.getContext());
                Logger log= Logger.getLogger("debug");

                String id="";
                try{
                    id=buttonView.getTag().toString();
                }catch(Exception ex){}
                String checked=""+isChecked;
                checked=checked.trim();

                String token="";
                try{
                    token= UserSession.getSingleton().getToken();
                }catch(Exception ex){}

                Procedure procedure=new PrivateUpdateSchedulersProcedure();
                SchedulerModel model=new SchedulerModel();
                model.setProcedure(procedure);
                model.setActivity(SchedulersActivity.ObjectSingleton.getSingleton().getSchedulersActivity());
                model.update(token,id,checked);
            }
        });

        return convertView;

    }
}
