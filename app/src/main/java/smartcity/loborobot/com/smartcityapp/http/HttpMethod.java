package smartcity.loborobot.com.smartcityapp.http;

import java.util.*;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */

public interface HttpMethod{
	public void 	setParams(List<String> params);
	public void 	setFields(List<String> fields);
    public List<String>  getFields();
	public void 	setUrl(String url);
	public String 	getUrl();
	public void 	send();
	public void 	execute();
	public String   getAnswer();
}

