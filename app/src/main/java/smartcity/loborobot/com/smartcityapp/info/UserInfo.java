package smartcity.loborobot.com.smartcityapp.info;


import java.util.*;

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */


public class UserInfo{
	
	private String name;
	private String lastname;
	private String nickname;
	private String token;
	private String email;

	private List<String> fields;
	
	public UserInfo(){
		super();
		fields=new ArrayList();
		fields.add("name");
		fields.add("lastname");
		fields.add("nickname");
		fields.add("token");
		fields.add("email");
	}

	public List<String> toData(){
		List<String> data=new ArrayList();

		HashMap d=toHashMap();
		String s="";
		for(String f: fields){
			try{
				s=d.get(f).toString();
			}catch(Exception ex){
				s="";
			}	
			data.add(s);
		}

		return data;
	}
	public HashMap toHashMap(){
		HashMap data=new HashMap();
		String val="";
		for(String f: fields){
			val="";			
			if(f.compareTo("name")==0){
				try{
					val=getName();
				}catch(Exception ex){
					val="";
				}
			}
			if(f.compareTo("lastname")==0){
				try{
					val=getLastName();
				}catch(Exception ex){
					val="";
				}
			}
			if(f.compareTo("nickname")==0){
				try{
					val=getNickName();
				}catch(Exception ex){
					val="";
				}
			}
			if(f.compareTo("token")==0){
				try{
					val=getToken();
				}catch(Exception ex){
					val="";
				}
			}
			if(f.compareTo("email")==0){
				try{
					val=getEmail();
				}catch(Exception ex){
					val="";
				}
			}
			if(val.compareTo("")!=0){
				data.put(f,val);	
			}			
		}

		return data;
	}

	public void setData(HashMap data){
		String val="";
		for(String f: fields){
			val="";
			try{
				val=data.get(f).toString();
			}catch(Exception ex){
				val="";
			}
			if(f.compareTo("name")==0){
				setName(val);
			}
			if(f.compareTo("lastname")==0){
				setLastName(val);
			}
			if(f.compareTo("nickname")==0){
				setNickName(val);
			}
			if(f.compareTo("token")==0){
				setToken(val);
			}
			if(f.compareTo("email")==0){
				setEmail(val);
			}
		}
	}	

	public List<String> getFields(){
		return fields;	
	}

	public String getName(){
		return name;
	}
	public void setName(String n){
		name=n;
	}

	public String getLastName(){
		return lastname;
	}
	public void setLastName(String n){
		lastname=n;
	}

	public String getNickName(){
		return nickname;
	}
	public void setNickName(String n){
		nickname=n;
	}

	public String getToken(){
		return token;
	}
	public void setToken(String n){
		token=n;
	}

	public String getEmail(){
		return email;
	}
	public void setEmail(String n){
		email=n;
	}
}
