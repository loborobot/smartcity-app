package smartcity.loborobot.com.smartcityapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */
public class MapPlaceholderFragment extends MapFragment {
    private GoogleMap mGoogleMap;



    private LayoutInflater inflater;
    public void setLayoutInflater(LayoutInflater l){
        inflater=l;
    }
    public LayoutInflater getLayoutInflater(){
        return inflater;
    }
    private ViewGroup container;
    public void setViewGroup(ViewGroup v){
        container=v;
    }
    public ViewGroup getViewGroup(){
        return container;
    }
    private Bundle savedInstanceState;

    public void setBundle(Bundle b){
        savedInstanceState=b;
    }
    public Bundle getBundle(){
        return savedInstanceState;
    }


    private static MainActivity activity;

    public static void setMainActivity(MainActivity a){
        activity=a;
    }
    public static MainActivity getMainActivity(){
        return activity;
    }

    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static MapPlaceholderFragment newInstance(int sectionNumber) {
        MapPlaceholderFragment fragment = new MapPlaceholderFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public MapPlaceholderFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        //FrameLayout layout=(FrameLayout) rootView.findViewById(R.id.container);

        //Toast.makeText(getActivity().getApplicationContext(), "Se hizo algo", Toast.LENGTH_SHORT).show();

        SupportMapFragment fragment = ( SupportMapFragment) getMainActivity().getSupportFragmentManager().findFragmentById(R.id.mapPublic);

        mGoogleMap = fragment.getMap();

        mGoogleMap.setMyLocationEnabled(true);

        try {
            mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(-16.3992433, -71.5369622)).title("Arequipa"));
        }catch(Exception ex){}

        return rootView;
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }

}
