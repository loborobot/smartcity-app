package smartcity.loborobot.com.smartcityapp.callable;

import android.app.Activity;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import smartcity.loborobot.com.smartcityapp.MainActivity;
import smartcity.loborobot.com.smartcityapp.extra.MainPrivateActivity;
import smartcity.loborobot.com.smartcityapp.extra.maps.MarkerInfo;
import smartcity.loborobot.com.smartcityapp.extra.session.DeviceSession;
import smartcity.loborobot.com.smartcityapp.info.DeviceInfo;
import smartcity.loborobot.com.smartcityapp.json.JSONDecoderAll;
import smartcity.loborobot.com.smartcityapp.message.Message;
import smartcity.loborobot.com.smartcityapp.model.SchedulerModel;
import smartcity.loborobot.com.smartcityapp.model.SensorModel;

/**
 * Created by fincyt on 13/01/15.
 */

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */
public class PrivateDevicesProcedure implements Procedure{
    /**
     * data in text to decode
     * */
    private Activity main;
    public void setActivity(Activity a){
        main=a;
    }
    public Activity getActivity(){
        return main;
    }
    private String data;
    public void setData(String d){
        data=d;
    }
    public String getData(){
        return data;
    }
    private List<String> fields=new ArrayList();
    public void setFields(List<String> fs){
        for(String s:fs){
            fields.add(s);
        }
    }
    public List<String> getFields(){
        return fields;
    }
    private List objects=new ArrayList();
    public void setObjects(List os){
        for(Object o:os){
            objects.add(o);
        }
    }
    public List getObjects(){
        return objects;
    }
    public void execute(){
        String dataString=getData();

        Message.getSingleton().setContext(getActivity().getApplicationContext());


        List<HashMap> data=new ArrayList();
        try{
            data= JSONDecoderAll.decode(dataString, getFields());
        }catch(Exception ex){}


        for(HashMap d:data){
            DeviceInfo device=new DeviceInfo();
            device.setData(d);
            LatLng latlng=new LatLng(
                    Double.parseDouble(
                            device.getLatitude()
                    ),
                    Double.parseDouble(
                            device.getLongitude()
                    )
            );
            String title=device.getName();
            Marker marker=null;
            MarkerInfo markerinfo=new MarkerInfo();
            try {
                marker=MainPrivateActivity.PlaceholderPrivateFragment.MapContainer.getSingleton().getMap().addMarker(
                        new MarkerOptions()
                                .position(latlng)
                                .title(title)
                );
                markerinfo.setMarker(marker);
                markerinfo.setId(device.getId());
            } catch (Exception ex) {

            }

            MainPrivateActivity.PlaceholderPrivateFragment.MarkersContainer.getSingleton().add(markerinfo);

            objects.add(device);

            ///Message.getSingleton().show(objects.toString());


            MainPrivateActivity.PlaceholderPrivateFragment.MapContainer.getSingleton().getMap().setOnMarkerClickListener(
                    new GoogleMap.OnMarkerClickListener() {

                        @Override
                        public boolean onMarkerClick(Marker arg0) {

                            Message.getSingleton().setContext(MainPrivateActivity.ActivityContainer.getSingleton().getActivity().getApplicationContext());

                            MarkerInfo marker=MainPrivateActivity.PlaceholderPrivateFragment.MarkersContainer.getSingleton().searchByMarkerId(arg0.getId());

                            //Message.getSingleton().show("Marker: "+marker.getId());

                            DeviceSession.getSingleton().setId(marker.getId());

                            Procedure procedure=new PrivateDeviceSensorsProcedure();
                            SensorModel model=new SensorModel();
                            model.setProcedure(procedure);
                            model.setActivity(getActivity());
                            model.getPrivateSensors();

                            return true;
                        }

                    }
            );
        }
    }
}

