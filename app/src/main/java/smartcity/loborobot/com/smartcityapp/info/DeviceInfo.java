package smartcity.loborobot.com.smartcityapp.info;

import java.util.*;

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */

public class DeviceInfo{

	private String token;
    private String name;
	private String dnsname;
	private String date_register;
	private String city;
	private String country;
	private String latitude;
	private String longitude;
	private String state;

	private List<String> fields;
    private String id;

    public DeviceInfo(){
		super();
		fields=new ArrayList();
		fields.add("token");
        fields.add("id");
        fields.add("name");
		fields.add("dnsname");
		fields.add("date_register");
		fields.add("city");
		fields.add("country");
		fields.add("latitude");
		fields.add("longitude");
		fields.add("state");
	}
	public List<String> toData(){
		List<String> data=new ArrayList();

		HashMap d=toHashMap();
		String s="";
		for(String f: fields){
			try{
				s=d.get(f).toString();
			}catch(Exception ex){
				s="";
			}	
			data.add(s);
		}

		return data;
	}

    public HashMap toHashMap() {
        HashMap data = new HashMap();
        String val = "";
        for (String f : fields) {
            val = "";
            if (f.compareTo("id") == 0) {
                try {
                    val = getId();
                } catch (Exception ex) {
                    val = "";
                }
            }
            if (f.compareTo("token") == 0) {
                try {
                    val = getToken();
                } catch (Exception ex) {
                    val = "";
                }
            }
            if (f.compareTo("name") == 0) {
                try {
                    val = getName();
                } catch (Exception ex) {
                    val = "";
                }
            }
            if (f.compareTo("dnsname") == 0) {
                try {
                    val = getDnsName();
                } catch (Exception ex) {
                    val = "";
                }
            }
            if (f.compareTo("date_register") == 0) {
                try {
                    val = getDateRegister();
                } catch (Exception ex) {
                    val = "";
                }
            }
            if (f.compareTo("city") == 0) {
                try {
                    val = getCity();
                } catch (Exception ex) {
                    val = "";
                }
            }
            if (f.compareTo("country") == 0) {
                try {
                    val = getCountry();
                } catch (Exception ex) {
                    val = "";
                }
            }
            if (f.compareTo("latitude") == 0) {
                try {
                    val = getLatitude();
                } catch (Exception ex) {
                    val = "";
                }
            }
            if (f.compareTo("longitude") == 0) {
                try {
                    val = getLongitude();
                } catch (Exception ex) {
                    val = "";
                }
            }
            if (f.compareTo("state") == 0) {
                try {
                    val = getState();
                } catch (Exception ex) {
                    val = "";
                }
            }
            if (val.compareTo("") != 0) {
                data.put(f, val);
            }
        }

        return data;
    }

    public void setData(HashMap data) {
        String val = "";
        for (String f : fields) {
            val = "";
            try {
                val = data.get(f).toString();
            } catch (Exception ex) {
                val = "";
            }
            if (f.compareTo("id") == 0) {
                setId(val);
            }
            if (f.compareTo("token") == 0) {
                setToken(val);
            }
            if (f.compareTo("name") == 0) {
                setName(val);
            }
            if (f.compareTo("dnsname") == 0) {
                setDnsName(val);
            }
            if (f.compareTo("date_register") == 0) {
                setDateRegister(val);
            }
            if (f.compareTo("city") == 0) {
                setCity(val);
            }
            if (f.compareTo("country") == 0) {
                setCountry(val);
            }
            if (f.compareTo("latitude") == 0) {
                setLatitude(val);
            }
            if (f.compareTo("longitude") == 0) {
                setLongitude(val);
            }
            if (f.compareTo("state") == 0) {
                setState(val);
            }
        }
    }

	public List<String> getFields(){
		return fields;	
	}

	public String getToken(){
		return token;
	}
	public void setToken(String n){
		token=n;
	}

	public String getDnsName(){
		return dnsname;
	}
	public void setDnsName(String n){
		dnsname=n;
	}

	public String getDateRegister(){
		return date_register;
	}
	public void setDateRegister(String n){
		date_register=n;
	}

	public String getCity(){
		return city;
	}
	public void setCity(String n){
		city=n;
	}

	public String getCountry(){
		return country;
	}
	public void setCountry(String n){
		country=n;
	}
	public String getLatitude(){
		return latitude;
	}
	public void setLatitude(String n){
		latitude=n;
	}
	public String getLongitude(){
		return longitude;
	}
	public void setLongitude(String n){
		longitude=n;
	}
	public String getState(){
		return state;
	}
	public void setState(String n){
		state=n;
	}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
