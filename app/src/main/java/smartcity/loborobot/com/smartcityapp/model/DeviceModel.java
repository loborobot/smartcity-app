
package smartcity.loborobot.com.smartcityapp.model;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

import smartcity.loborobot.com.smartcityapp.MainActivity;
import smartcity.loborobot.com.smartcityapp.callable.*;
import smartcity.loborobot.com.smartcityapp.extra.SchedulersActivity;
import smartcity.loborobot.com.smartcityapp.http.*;
import smartcity.loborobot.com.smartcityapp.info.*;

import smartcity.loborobot.com.smartcityapp.message.*;
import smartcity.loborobot.com.smartcityapp.model.*;

import smartcity.loborobot.com.smartcityapp.async.*;
import smartcity.loborobot.com.smartcityapp.json.*;
import smartcity.loborobot.com.smartcityapp.session.UserSession;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import org.json.*;

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */

public class DeviceModel{
	private Activity main;
	private DeviceInfo info;
    private Procedure procedure;

    public void setProcedure(Procedure p){
        procedure=p;
    }
	public Procedure getProcedure() {
        return procedure;
    }
	public void setActivity(Activity a){
		main=a;
	}
	public Activity getActivity(){
		return main;
	}

	public DeviceModel(){
		info=new DeviceInfo();
	}

	public void getAll(){ // metodo get

        String dataString="";

		List<DeviceInfo> devices=new ArrayList();
		String url=HttpBaseURL.getBaseUrl()+"webapi/devices/?format=json";

        Message.getSingleton().setContext(MainActivity.ActivityContainer.getSingleton().getActivity().getApplicationContext());

        //Message.getSingleton().show(url);

		List<String> params=new ArrayList();
		List<String> fields=info.getFields();

		List<HashMap> data=new ArrayList();		

		HttpMethod method=new HttpGetMethod();
		method.setFields(fields);
		method.setParams(params);
		method.setUrl(url);/**/

		AsyncCallExecute async=new AsyncCallExecute();
		async.setMethod(method);
        async.setProcedure(getProcedure());//esto va despues del metodo siempre
		async.setActivity(getActivity());
		async.execute();


	}



    public void getPrivateAll(){ // metodo get

        String dataString="";

        List<DeviceInfo> devices=new ArrayList();
        String url=HttpBaseURL.getBaseUrl()+"tokenapi/devicetoken/?format=json";

        String token="";
        try{
            token= UserSession.getSingleton().getToken();
        }catch(Exception ex){}

        List<String> params=new ArrayList();
        params.add(token);
        List<String> fields=info.getFields();

        List<HashMap> data=new ArrayList();

        HttpMethod method=new HttpGetMethod();
        method.setFields(fields);
        method.setParams(params);
        method.setUrl(url);/**/

        AsyncCallExecute async=new AsyncCallExecute();
        async.setMethod(method);
        async.setProcedure(getProcedure());//esto va despues del metodo siempre
        async.setActivity(getActivity());
        async.execute();


    }



	public DeviceInfo getById(String id){ // metodo get
		DeviceInfo device=new DeviceInfo();
		String url=HttpBaseURL.getBaseUrl()+"webapi/device/"+id+"/";

		List<String> params=new ArrayList();
		List<String> fields=info.getFields();

		HashMap data=new HashMap();		

		HttpMethod method=new HttpGetMethod();
		method.setFields(fields);
		method.setParams(params);
		method.setUrl(url);

		AsyncCallExecute async=new AsyncCallExecute();
		async.setMethod(method);
		async.setActivity(getActivity());
		async.execute();

		String dataString=async.getData();
		try{
			data=JSONDecoder.decode(dataString,fields);
		}catch(Exception ex){}		
		
		device.setData(data);		

		return device;
	}

	public HashMap getLatLng(String id){ // metodo get
		HashMap data=new HashMap();
		DeviceInfo device=new DeviceInfo();
		String url=HttpBaseURL.getBaseUrl()+"webapi/device/"+id+"/";

		List<String> params=new ArrayList();
		List<String> fields=info.getFields();

		HttpMethod method=new HttpGetMethod();
		method.setFields(fields);
		method.setParams(params);
		method.setUrl(url);

		AsyncCallExecute async=new AsyncCallExecute();
		async.setMethod(method);
		async.setActivity(getActivity());
		async.execute();

		String dataString=async.getData();
		HashMap d=new HashMap();
		try{
			d=JSONDecoder.decode(dataString,fields);
		}catch(Exception ex){}		
		
		device.setData(d);
		String lat="";
		String lng="";
		if(device.getLatitude().compareTo("")!=0){
			lat=device.getLatitude();
		}
		if(device.getLongitude().compareTo("")!=0){
			lng=device.getLongitude();
		}
		data.put("lat",lat);
		data.put("lng",lng);
		return data;
	}

}
