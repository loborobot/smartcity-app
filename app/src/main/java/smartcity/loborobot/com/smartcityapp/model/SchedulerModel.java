
package smartcity.loborobot.com.smartcityapp.model;

import java.util.*;
import java.util.logging.Logger;

import smartcity.loborobot.com.smartcityapp.callable.Procedure;
import smartcity.loborobot.com.smartcityapp.extra.SchedulersActivity;
import smartcity.loborobot.com.smartcityapp.http.*;

import smartcity.loborobot.com.smartcityapp.info.*;

import smartcity.loborobot.com.smartcityapp.message.Message;
import smartcity.loborobot.com.smartcityapp.model.*;

import smartcity.loborobot.com.smartcityapp.async.*;

import smartcity.loborobot.com.smartcityapp.json.*;
import smartcity.loborobot.com.smartcityapp.session.UserSession;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import org.json.*;

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */

public class SchedulerModel{

	private Activity main;
	private SchedulerInfo info;
    private Procedure procedure;

    public void setActivity(Activity a){
		main=a;
	}
	public Activity getActivity(){
		return main;
	}

    public void setProcedure(Procedure p){
        procedure=p;
    }
    public Procedure getProcedure(){
        return procedure;
    }

	public SchedulerModel(){
		info=new SchedulerInfo();
	}

	public DeviceInfo getDeviceByScheduler(String idScheduler){
		DeviceInfo device=new DeviceInfo();

		SchedulerInfo scheduler=new SchedulerInfo();

		scheduler=getById(idScheduler);

		String id=scheduler.getDevice();

		DeviceModel devicemodel = new DeviceModel();

		device=devicemodel.getById(id);

		return device;
	}
    //**
    // obtener data de todo los schedulers
    //
    // */
	public void getAll(){//metodo funcional

        String dataString="";

        //List< SchedulerInfo > schedulers=new ArrayList();
        String url=HttpBaseURL.getBaseUrl()+"webapi/schedulers/?format=json";
        List<String> params=new ArrayList();
        List<String> fields=info.getFields();

        List<HashMap> data=new ArrayList();

        HttpMethod method=new HttpGetMethod();
        method.setFields(fields);
        method.setParams(params);
        method.setUrl(url);/**/

        AsyncCallExecute async=new AsyncCallExecute();
        async.setMethod(method);
        async.setProcedure(getProcedure());//esto va despues del metodo siempre
        async.setActivity(getActivity());
        async.execute();

	}

    public void getPrivateAll(){//metodo funcional

        String dataString="";

        //List< SchedulerInfo > schedulers=new ArrayList();

        Message.getSingleton().setContext(SchedulersActivity.ObjectSingleton.getSingleton().getActivity());
        String url=HttpBaseURL.getBaseUrl()+"tokenapi/schedulertoken/?format=json";

        //Message.getSingleton().show(url);

        Logger log= Logger.getLogger("debug");
        //log.info("url: "+url);

        String token="";
        try{
            token= UserSession.getSingleton().getToken();
        }catch(Exception ex){}

        List<String> params=new ArrayList();
        //param token
        params.add(token);

        List<String> fields=info.getFields();

        ///Message.getSingleton().show("token: "+token);

        log.info(fields.toString());

        List<HashMap> data=new ArrayList();

        HttpMethod method=new HttpGetMethod();
        method.setFields(fields);
        method.setParams(params);
        method.setUrl(url);/**/

        AsyncCallExecute async=new AsyncCallExecute();
        async.setMethod(method);
        async.setProcedure(getProcedure());//esto va despues del metodo siempre
        async.setActivity(getActivity());
        async.execute();

    }

	public void update(String token, String id,String enable){

        Logger log= Logger.getLogger("debug");

		String url=HttpBaseURL.getBaseUrl()+"tokenapi/schedulerupdate/?format=json";

		List<SchedulerInfo> schedulers=new ArrayList();

        log.info(url);
		
		List<String> params=new ArrayList();
        params.add(token);
        params.add(id);
        params.add("");
        params.add("");
        params.add("");
        params.add("");
        params.add("");
        params.add("");
        params.add("");
        params.add("");
        params.add(enable);//parametro nro 11 enabled
        params.add("");
		List<String> fields=info.getFields();

		HttpMethod method=new HttpGetMethod();
		method.setFields(fields);
		method.setParams(params);
		method.setUrl(url);

		AsyncCallExecute async=new AsyncCallExecute();
		async.setMethod(method);
		async.setActivity(getActivity());
		async.execute();	

	}

	public void insert(SchedulerInfo scheduler){

		String url=HttpBaseURL.getBaseUrl()+"webapi/schedulers/?format=json";

		List<String> params=new ArrayList();
		List<String> fields=info.getFields();

		List<HashMap> data=new ArrayList();		

		params=scheduler.toData();

		HttpMethod method=new HttpPostMethod();
		method.setFields(fields);
		method.setParams(params);
		method.setUrl(url);

		AsyncCallExecute async=new AsyncCallExecute();
		async.setMethod(method);
		async.setActivity(getActivity());
		async.execute();
		
	}


	public SchedulerInfo getById(String id){
		String url=HttpBaseURL.getBaseUrl()+"webapi/schedulers/"+id+"/?format=json";

		SchedulerInfo answer=new SchedulerInfo();


		List<String> params=new ArrayList();
		List<String> fields=info.getFields();

		HashMap data=new HashMap();		

		HttpMethod method=new HttpGetMethod();
		method.setFields(fields);
		method.setParams(params);
		method.setUrl(url);

		AsyncCallExecute async=new AsyncCallExecute();
		async.setMethod(method);
		async.setActivity(getActivity());
		async.execute();

		String dataString=async.getData();
		try{
			data=JSONDecoder.decode(dataString,fields);
		}catch(Exception ex){}		
		
		answer.setData(data);

		return answer;
	}
}