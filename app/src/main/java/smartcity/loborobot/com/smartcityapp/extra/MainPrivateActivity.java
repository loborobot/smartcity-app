package smartcity.loborobot.com.smartcityapp.extra;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.view.ViewOutlineProvider;
import android.widget.*;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import smartcity.loborobot.com.smartcityapp.R;

import smartcity.loborobot.com.smartcityapp.callable.DevicesProcedure;
import smartcity.loborobot.com.smartcityapp.callable.PrivateDeviceSensorsProcedure;
import smartcity.loborobot.com.smartcityapp.callable.PrivateDevicesProcedure;
import smartcity.loborobot.com.smartcityapp.callable.PrivateSchedulersProcedure;
import smartcity.loborobot.com.smartcityapp.callable.Procedure;
import smartcity.loborobot.com.smartcityapp.database.Database;
import smartcity.loborobot.com.smartcityapp.database.PrivateDatabase;
import smartcity.loborobot.com.smartcityapp.extra.data.*;
import smartcity.loborobot.com.smartcityapp.*;
import smartcity.loborobot.com.smartcityapp.extra.maps.MarkerInfo;
import smartcity.loborobot.com.smartcityapp.info.HistoryInfo;
import smartcity.loborobot.com.smartcityapp.message.Message;
import smartcity.loborobot.com.smartcityapp.model.DeviceModel;
import smartcity.loborobot.com.smartcityapp.model.SchedulerModel;

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */

public class MainPrivateActivity extends android.support.v7.app.ActionBarActivity
        implements NavigationDrawerPrivateFragment.NavigationDrawerPrivateCallbacks {

    /*
    * Activity container for Private Activity
    * */
    public static class ActivityContainer{
        private static ActivityContainer singleton;
        private static Activity activity;
        private static MainPrivateActivity main;

        public void setActivity(Activity a){
            activity=a;
        }
        public Activity getActivity(){
            return activity;
        }

        public void setMainPrivateActivity(MainPrivateActivity a){
            main=a;
        }
        public MainPrivateActivity getMainPrivateActivity(){
            return main;
        }


        private ActivityContainer(){

        }
        public static ActivityContainer getSingleton(){
            if(singleton==null){
                singleton=new ActivityContainer();
            }
            return singleton;
        }
    }
    /*
    * BDContainer for Private Activity
    * */
    public static class BDContainer{
        private static SQLiteDatabase db= null;
        private static BDContainer singleton;
        private BDContainer(){

        }
        public static  void setDb(SQLiteDatabase database){
            db=database;
        }
        public static SQLiteDatabase getDb(){
            return db;
        }
        public static BDContainer getSingleton(){
            if(singleton==null){
                singleton=new BDContainer();
            }
            return singleton;
        }
    }

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerPrivateFragment mNavigationDrawerPrivateFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;


    private void startActivityAfterCleanup(Class<?> cls) {
        Intent intent = null;
        try{
            intent=new Intent(MainPrivateActivity.this, cls);
        }catch(Exception ex){}
        try {
            if(intent!=null) {
                startActivity(intent);
            }
        }catch(Exception ex){}

    }

    public void onClickSwitchScheduler(View v){
        //Message.getSingleton().setContext(getApplicationContext());

        //Message.getSingleton().show("switch");

    }/**/

    /*
    * click logout
    * */
    public void onClickLogout(View v){
        startActivityAfterCleanup(MainActivity.class);
    }

    /*
    * click add scheduler
    * */

    public void onClickAddScheduler(View v){
        FragmentManager fragmentManager = getSupportFragmentManager();
        PlaceholderSchedulerPrivateFragment fragment=PlaceholderSchedulerPrivateFragment.newInstance(0);
        fragment.setMainPrivateActivity(this);
        fragmentManager.beginTransaction()
                .replace(R.id.containerPrivate,fragment )
                .commit();
    }

    /*
    * click back scheduler
    * */

    public void onClickBackScheduler(View v){
        FragmentManager fragmentManager = getSupportFragmentManager();
        PlaceholderSchedulersPrivateFragment fragment=PlaceholderSchedulersPrivateFragment.newInstance(0);
        fragment.setMainPrivateActivity(this);
        fragmentManager.beginTransaction()
                .replace(R.id.containerPrivate,fragment )
                .commit();
    }

    /*
    * Left show data in view
    *
    * */

    public void leftShowHistory(View v){

        //Message.getSingleton().setContext(MainPrivateActivity.ActivityContainer.getSingleton().getActivity().getApplicationContext());
        //Message.getSingleton().show("left");

        Message.getSingleton().setContext(MainPrivateActivity.ActivityContainer.getSingleton().getActivity().getApplicationContext());


        int index = PlaceholderHistoryPrivateFragment.SensorIndex.getSingleton().getIndex();
        index--;
        if(index<0){index=6;}

        //Message.getSingleton().show(" : "+index);

        List<HistoryInfo> list_histories =null;
        try{
            list_histories= PrivateDeviceSensorsProcedure.HistoryInfoContainer.getSingleton().getData();
        }catch(Exception ex){}
        HistoryInfo data=null;
        try{
            data=list_histories.get(index);
        }catch(Exception ex){}
        //Message.getSingleton().show("size: "+list_histories.size());
        String sensors[]={"temperature","humidity","sound","flowmeter","light","nitrogen_dioxide","carbon_monoxide"};
        String measures[]={"C°","%RH","DB","L/H","LUX","PBM","PPM"};
        if(data!=null) {
            TextView texto = null;
            try{
                texto=(TextView) findViewById(R.id.textViewHistory);
            }catch(Exception ex){}

            String label="";
            String measure="";
            String value="";
            try{
                label=sensors[index].toUpperCase();
            }catch(Exception ex){
                label="";
            }
            try{
                measure=measures[index].toUpperCase();
            }catch(Exception ex){
                measure="";
            }
            try{
                value=data.getValue();
            }catch(Exception ex){
                value="";
            }
            try{
                texto.setText(label+" : "+value+" "+measure);
            }catch(Exception ex){}

        }
        PlaceholderHistoryPrivateFragment.SensorIndex.getSingleton().setIndex(index);

    }

    /*
    * Right show data in view
    *
    * */

    public void rightShowHistory(View v){
        Message.getSingleton().setContext(MainPrivateActivity.ActivityContainer.getSingleton().getActivity().getApplicationContext());
        //Message.getSingleton().show("right");

        int index = PlaceholderHistoryPrivateFragment.SensorIndex.getSingleton().getIndex();
        index++;
        if(index>6){index=0;}
        List<HistoryInfo> list_histories =null;

        //Message.getSingleton().show(" : "+index);

        try{
            list_histories= PrivateDeviceSensorsProcedure.HistoryInfoContainer.getSingleton().getData();
        }catch(Exception ex){}
        HistoryInfo data=null;
        try{
            data=list_histories.get(index);
        }catch(Exception ex){}

        //Message.getSingleton().show("size: "+list_histories.size());

        String sensors[]={"temperature","humidity","sound","flowmeter","light","nitrogen_dioxide","carbon_monoxide"};
        String measures[]={"C°","%RH","DB","L/H","LUX","PBM","PPM"};
        if(data!=null) {
            TextView texto = null;
            try{
                texto=(TextView) findViewById(R.id.textViewHistory);
            }catch(Exception ex){}

            String label="";
            String measure="";
            String value="";
            try{
                label=sensors[index].toUpperCase();
            }catch(Exception ex){
                label="";
            }
            try{
                measure=measures[index].toUpperCase();
            }catch(Exception ex){
                measure="";
            }
            try{
                value=data.getValue();
            }catch(Exception ex){
                value="";
            }
            //Message.getSingleton().show("string: "+label);
            try{
                texto.setText(label+" : "+value+" "+measure);
            }catch(Exception ex){}

        }
        PlaceholderHistoryPrivateFragment.SensorIndex.getSingleton().setIndex(index);
    }

    /*
    * click add action in form scheduler
    *
    * */

    public void onClickActionAddScheduler(View v) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        PlaceholderSchedulersPrivateFragment fragment = PlaceholderSchedulersPrivateFragment.newInstance(0);
        fragment.setMainPrivateActivity(this);
        fragmentManager.beginTransaction()
                .replace(R.id.containerPrivate, fragment)
                .commit();

        EditText editNombre=null;
        try{
            editNombre=(EditText) v.findViewById(R.id.editNombre);
        }catch(Exception ex){}
        /*
        TimePicker timeStartPicker=null;
        try{
            timeStartPicker=(TimePicker) v.findViewById(R.id.timeStartPicker);
        }catch(Exception ex){}
        TimePicker timeEndPicker=null;
        try{
            timeEndPicker=(TimePicker) v.findViewById(R.id.timeEndPicker);
        }catch(Exception ex){}

        DatePicker dateStartPicker=null;
        try{
            dateStartPicker= (DatePicker) v.findViewById(R.id.dateStartPicker);
        }catch(Exception ex){}
        DatePicker dateEndPicker=null;
        try{
            dateEndPicker=(DatePicker) v.findViewById(R.id.dateEndPicker);
        }catch(Exception ex){}/**/

        CheckBox checkBoxRepetir=null;
        try{
            checkBoxRepetir=(CheckBox) v.findViewById(R.id.checkBoxRepetir);
        }catch(Exception ex){}

        ToggleButton monday=null;
        ToggleButton tuesday=null;
        ToggleButton wednesday=null;
        ToggleButton thursday=null;
        ToggleButton friday=null;
        ToggleButton saturday=null;
        ToggleButton sunday=null;

        try{
            monday=(ToggleButton) v.findViewById(R.id.toggleMonday);
        }catch(Exception ex){}
        try{
            tuesday=(ToggleButton) v.findViewById(R.id.toggleTuesday);
        }catch(Exception ex){}
        try{
            wednesday=(ToggleButton) v.findViewById(R.id.toggleWednesday);
        }catch(Exception ex){}
        try{
            thursday=(ToggleButton) v.findViewById(R.id.toggleThursday);
        }catch(Exception ex){}
        try{
            friday=(ToggleButton) v.findViewById(R.id.toggleFriday);
        }catch(Exception ex){}
        try{
            saturday=(ToggleButton) v.findViewById(R.id.toggleSaturday);
        }catch(Exception ex){}
        try{
            sunday=(ToggleButton) v.findViewById(R.id.toggleSunday);
        }catch(Exception ex){}



    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.extra_activity_main);

        mNavigationDrawerPrivateFragment = (NavigationDrawerPrivateFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer_private);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerPrivateFragment.setUp(
                R.id.navigation_drawer_private,
                (DrawerLayout) findViewById(R.id.drawer_layout_private));

        SQLiteDatabase db=null;
        try{
            db=openOrCreateDatabase("SmartCityDB.db", Context.MODE_PRIVATE,null);
        }catch(Exception ex){}
        BDContainer.getSingleton().setDb(db);

        PrivateDatabase.getSingleton().createTableUser();
        PrivateDatabase.getSingleton().insertUserDummy();

        HashMap data=PrivateDatabase.getSingleton().getUser();

        ActivityContainer.getSingleton().setActivity(this);

        //Message.getSingleton().setContext(MainPrivateActivity.ActivityContainer.getSingleton().getActivity().getApplicationContext());
        //Message.getSingleton().show(data.get("username").toString());

        String success=data.get("success").toString();

        if(success.compareTo("true")!=0){
            Intent intent = null;
            try{
                intent=new Intent(MainPrivateActivity.ActivityContainer.getSingleton().getActivity().getApplicationContext(), MainActivity.class);
            }catch(Exception ex){}
            try {
                if(intent!=null) {
                    MainPrivateActivity.ActivityContainer.getSingleton().getActivity().startActivity(intent);
                }
            }catch(Exception ex){}
        }


    }
    public static class FragmentManagerContainer{
        private static FragmentManager fragmentManager;
        private static FragmentManagerContainer singleton;
        private FragmentManagerContainer(){

        }
        public static void setFragmentManager(FragmentManager manager){
            fragmentManager=manager;
        }
        public static FragmentManager getFragmentManager(){
            return fragmentManager;
        }
        public static FragmentManagerContainer getSingleton() {
            if(singleton==null){
                singleton=new FragmentManagerContainer();
            }
            return singleton;
        }
    }
    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentManagerContainer.getSingleton().setFragmentManager(fragmentManager);
        PlaceholderPrivateFragment fragment=PlaceholderPrivateFragment.newInstance(position + 1);
        fragment.setMainPrivateActivity(this);
        fragmentManager.beginTransaction()
                .replace(R.id.containerPrivate,fragment )
                .commit();
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(
                                R.string.title_sectionLogout
                         );
                SingletonMenu.getSingletonMenu().setOption(1);

                break;
            case 2:
                mTitle = getString(R.string.title_sectionPrivateDevices);
                SingletonMenu.getSingletonMenu().setOption(2);
                break;
            case 3:
                mTitle = getString(R.string.title_sectionPrivateSchedulers);
                SingletonMenu.getSingletonMenu().setOption(3);
                break;
            default:
                mTitle = getString(R.string.title_sectionDevices);
                SingletonMenu.getSingletonMenu().setOption(2);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerPrivateFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.private_main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings_private) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public static class AdapterContainer {
        private static SchedulerAdapter adapter = null;
        public static void setSchedulerAdapter(SchedulerAdapter sa){
            adapter=sa;
        }
        public static SchedulerAdapter getSchedulerAdapter(){
            return adapter;
        }
        private static AdapterContainer singleton=null;
        public AdapterContainer(){

        }
        private  static AdapterContainer getSingleton(){
            if(singleton==null){
                singleton=new AdapterContainer();
            }
            return singleton;
        }

    }
    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderPrivateFragment extends Fragment {

        private static LatLng AQP=null;

        public void setSupportMapFragment(SupportMapFragment supportMapFragment) {
            this.supportMapFragment = supportMapFragment;
        }


        public static class MarkersContainer{
            private static List<MarkerInfo> markers=null;
            private static MarkersContainer singleton=null;

            public static MarkersContainer getSingleton(){
                if(singleton==null){
                    singleton=new MarkersContainer();
                }
                return singleton;
            }
            private MarkersContainer(){
                markers=new ArrayList();
            }
            public void add(MarkerInfo m){
                markers.add(m);
            }
            public void setMarkers(List<MarkerInfo> lm){
                for(MarkerInfo m:lm){
                    markers.add(m);
                }
            }
            public MarkerInfo getMarker(int index){
                return (MarkerInfo) getMarkers().get(index);
            }
            public List<MarkerInfo> getMarkers(){
                return markers;
            }
            //*
            // Searching marker info taking id of marker info
            // This gets MarkerInfo taking into account id of device
            // *//
            public MarkerInfo searchMarkerById(String id){
                MarkerInfo result=null;
                for(MarkerInfo m:getMarkers()){
                    if(m.getId().compareTo(id)==0){
                        result=m;
                    }
                }
                return result;
            }
            //*
            // Searching marker info taking id of marker
            // This gets an object marker by its id
            // *//
            public MarkerInfo searchByMarkerId(String id){
                MarkerInfo result=null;
                for(MarkerInfo m:getMarkers()){
                    if(m.getMarker().getId().compareTo(id)==0){
                        result=m;
                    }
                }
                return result;
            }
        }

        public static  class LayoutInflaterContainer{
            private static  LayoutInflaterContainer singleton;
            private static LayoutInflater inflater;
            public LayoutInflaterContainer(){

            }
            public static LayoutInflaterContainer getSingleton(){
                if(singleton==null){
                    singleton=new LayoutInflaterContainer();
                }
                return singleton;
            }
            public static void setLayoutInflater(LayoutInflater l){
                inflater=l;
            }
            public static LayoutInflater getLayoutInflater(){
                return inflater;
            }
        }

        public static class ListViewContainer{
            private static  ListViewContainer singleton;
            private static ListView view;
            public ListViewContainer(){

            }
            public static ListViewContainer getSingleton(){
                if(singleton==null){
                    singleton=new ListViewContainer();
                }
                return singleton;
            }
            public static void setListView(ListView v){
                view=v;
            }
            public static ListView getListView(){
                return view;
            }
        }

        public static class MapContainer{
            private static MapContainer singleton;
            private static GoogleMap mGoogleMap;
            private SupportMapFragment supportMapFragment=null;
            public void setSupportMapFragment(SupportMapFragment smf){
                supportMapFragment=smf;
                try{
                    mGoogleMap = getSupportMapFragment().getMap();
                }catch(Exception ex){}
            }
            public SupportMapFragment getSupportMapFragment(){
                return supportMapFragment;
            }
            public GoogleMap getMap(){
                return mGoogleMap;
            }
            private MapContainer(){

            }
            public static MapContainer getSingleton(){
                if(singleton==null){
                    singleton=new MapContainer();
                }
                return singleton;
            }

        }

        private static GoogleMap mGoogleMap;

        private SupportMapFragment supportMapFragment=null;

        private LayoutInflater inflater;
        public void setLayoutInflater(LayoutInflater l){
            inflater=l;
        }
        public LayoutInflater getLayoutInflater(){
            return inflater;
        }
        private ViewGroup container;
        public void setViewGroup(ViewGroup v){
            container=v;
        }
        public ViewGroup getViewGroup(){
            return container;
        }
        private Bundle savedInstanceState;

        public void setBundle(Bundle b){
            savedInstanceState=b;
        }
        public Bundle getBundle(){
            return savedInstanceState;
        }

        private ListView listViewSchedulers;

        private static MainPrivateActivity activity;

        public static void setMainPrivateActivity(MainPrivateActivity a){
            activity=a;
        }
        public static MainPrivateActivity getMainPrivateActivity(){
            return activity;
        }

        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderPrivateFragment newInstance(int sectionNumber) {
            PlaceholderPrivateFragment fragment = new PlaceholderPrivateFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderPrivateFragment() {
            AQP=new LatLng(-16.3992433, -71.5369622);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            setLayoutInflater(inflater);
            setViewGroup(container);
            setBundle(savedInstanceState);

            View rootView =null;
            int opcion=SingletonMenu.getSingletonMenu().getOption();
            if(opcion==1){
                rootView=onCreateLogoutView(inflater,container,savedInstanceState);
            }
            if(opcion==2){
                rootView=onCreateMainView(inflater,container,savedInstanceState);
            }
            if(opcion==3){
                rootView=onCreateSchedulersView(inflater,container,savedInstanceState);
            }
            return rootView;
        }

        public View onCreateLogoutView(LayoutInflater inflater, ViewGroup container,
                                       Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.extra_fragment_logout, container, false);
            return rootView;
        }

        public View onCreateMainView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.extra_fragment_main, container, false);

            FragmentActivity activity=getActivity();
            FragmentManager manager=activity.getSupportFragmentManager();

            List<Fragment> fragments = new ArrayList();
            try {
                fragments = manager.getFragments();
            }catch(Exception ex){}

            FragmentManager fr=null;
            SupportMapFragment smf=null;
            for(Fragment f:fragments){
                try{fr=f.getChildFragmentManager();}catch(Exception ex){}
                try{smf=(SupportMapFragment) fr.findFragmentById(R.id.mapPrivate);}catch(Exception ex){smf=null;}
                if(smf!=null){
                    try{
                        setSupportMapFragment(smf);
                        MapContainer.getSingleton().setSupportMapFragment(smf);
                    }catch(Exception ex){}
                    try{
                        //mGoogleMap = supportMapFragment.getMap();
                    }catch(Exception ex){}
                    if(MapContainer.getSingleton().getMap()!=null) {
                        try {
                            MapContainer.getSingleton().getMap().setMyLocationEnabled(true);
                        } catch (Exception ex) {
                        }
                        try {
////                            MapContainer.getSingleton().getMap().addMarker(new MarkerOptions().position(AQP).title("Arequipa"));
                        } catch (Exception ex) {
                        }
                        try {
                            MapContainer.getSingleton().getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(AQP, 14));
                        } catch (Exception ex) {
                        }
                        try {
                            MapContainer.getSingleton().getMap().animateCamera(CameraUpdateFactory.zoomBy(18));
                        } catch (Exception ex) {
                        }
                        try {
                            MapContainer.getSingleton().getMap().moveCamera(CameraUpdateFactory.newLatLng(AQP));
                        } catch (Exception ex) {
                        }
                        try{
                            MapContainer.getSingleton().getMap().setMapType(GoogleMap.MAP_TYPE_HYBRID);
                        }catch(Exception ex){}

                        Procedure procedure=new PrivateDevicesProcedure();
                        DeviceModel model=new DeviceModel();
                        model.setProcedure(procedure);
                        model.setActivity(getActivity());
                        model.getPrivateAll();
                    }
                }
            }
            
            return rootView;
        }



        public View onCreateSchedulersView(LayoutInflater inflater, ViewGroup container,
                                     Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.extra_fragment_schedulers, container, false);

            try {
                listViewSchedulers = (ListView) rootView.findViewById(R.id.gridViewSchedulers);
                /*listViewSchedulers.setOnItemClickListener(
                    new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Message.getSingleton().setContext(MainPrivateActivity.ActivityContainer.getSingleton().getActivity().getApplicationContext());
                            Message.getSingleton().show("Hola este es un tap");
                        }
                    }
                );*/
                ListViewContainer.getSingleton().setListView(listViewSchedulers);
            }catch(Exception ex){}

            try{
                LayoutInflaterContainer.getSingleton().setLayoutInflater(inflater);
            }catch(Exception ex){}
/*
            Procedure procedure=new PrivateSchedulersProcedure();
            SchedulerModel model=new SchedulerModel();
            model.setProcedure(procedure);
            model.setActivity(getActivity());
            model.getAll();*/

            try{
                rootView=onCreateMainView(inflater,container,savedInstanceState);
            }catch(Exception ex){}


            Intent intent = null;
            try{
                intent=new Intent(MainActivity.ActivityContainer.getSingleton().getActivity().getApplicationContext(), SchedulersActivity.class);
            }catch(Exception ex){}
            try {
                if(intent!=null) {
                    MainActivity.ActivityContainer.getSingleton().getActivity().startActivity(intent);
                }
            }catch(Exception ex){}

            /*SchedulerData scheduler_data[] = new SchedulerData[]
                    {
                            new SchedulerData("Scheduler 1",true),
                            new SchedulerData("Scheduler 2",false),
                            new SchedulerData("Scheduler 3",true)
                    };

            SchedulerAdapter adapter=null;
            try {
                adapter = new SchedulerAdapter(getMainPrivateActivity().getBaseContext(), R.layout.extra_schedulers_listview_item_row, scheduler_data);
                if(adapter!=null){
                    adapter.setMainPrivateActivity(getMainPrivateActivity());
                }

            }catch(Exception ex){}

            try {
                listViewSchedulers = (ListView) rootView.findViewById(R.id.gridViewSchedulers);
            }catch(Exception ex){}
            //View header = (View)inflater.inflate(R.layout.extra_schedulers_listview_header_row, null);
            //listViewSchedulers.addHeaderView(header);
            try{
                listViewSchedulers.setAdapter(adapter);
            }catch(Exception ex){}*/

            return rootView;
        }



        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainPrivateActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }

}
