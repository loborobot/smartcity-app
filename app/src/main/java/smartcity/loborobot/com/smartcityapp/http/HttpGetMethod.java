package smartcity.loborobot.com.smartcityapp.http;

import java.util.*;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */

 public class HttpGetMethod implements HttpMethod{

	protected List params=new ArrayList();
	protected List<String> fields=new ArrayList();
	protected String url="";
	protected String method="";
	protected String answer="";

	public HttpGetMethod(){
		super();
		method="get";

	}
	public void setParams(List<String> ps){
		String p="";
		int cont=0;
		for(Object f : fields){		
			p="";
			try{
				p=ps.get(cont);
			}catch(Exception ex){
				p="";
			}			
			HashMap d=new HashMap();
			d.put("field",f.toString());
			d.put("data",p);
			cont++;
			params.add(d);
		}
	}
	public void setFields(List<String> fs){
		for(String f : fs){
			fields.add(f);
		}
	}
    public List<String> getFields(){
        return fields;
    }
	public void setUrl(String u){
		url=u;
	}
	public String getUrl(){
		return url;
	}	
	public void send(){
        try{
            answer=HttpGetAction.send(params,url);
        }catch(Exception ex){}
	}
	public void execute(){
		send();
	}
	public String getAnswer(){
		return answer;
	}
	
}
