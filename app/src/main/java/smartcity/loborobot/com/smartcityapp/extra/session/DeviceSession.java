package smartcity.loborobot.com.smartcityapp.extra.session;

/**
 * Created by fincyt on 23/01/15.
 */

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */

 public class DeviceSession {
    private static String id="";
    private static DeviceSession singleton;
    private DeviceSession(){
        id="";
    }
    public static DeviceSession getSingleton(){
        if(singleton==null){
            singleton=new DeviceSession();
        }
        return singleton;
    }
    public static String getId(){
        return id;
    }
    public static void setId(String i){
        id=i;
    }
}
