
package smartcity.loborobot.com.smartcityapp.model;

import java.util.*;

import smartcity.loborobot.com.smartcityapp.MainActivity;
import smartcity.loborobot.com.smartcityapp.callable.Procedure;
import smartcity.loborobot.com.smartcityapp.extra.MainPrivateActivity;
import smartcity.loborobot.com.smartcityapp.extra.session.DeviceSession;
import smartcity.loborobot.com.smartcityapp.http.*;

import smartcity.loborobot.com.smartcityapp.info.*;

import smartcity.loborobot.com.smartcityapp.message.Message;
import smartcity.loborobot.com.smartcityapp.model.*;

import smartcity.loborobot.com.smartcityapp.async.*;

import smartcity.loborobot.com.smartcityapp.json.*;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import org.json.*;

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */

public class SensorModel{

	private Activity main;
	private SensorInfo info;
    private HistoryInfo history;
    private Procedure procedure;
	
	public void setActivity(Activity a){
		main=a;
	}
	public Activity getActivity(){
		return main;
	}

    public void setProcedure(Procedure p){
        procedure=p;
    }
    public Procedure getProcedure(){
        return procedure;
    }

	public SensorModel(){
		info=new SensorInfo();
        history=new HistoryInfo();
	}

    public void getPublicSensors(){

        String dataString="";
        List<HistoryInfo> sensors=new ArrayList();
        String url=HttpBaseURL.getBaseUrl()+"tokenapi/sensorstoken/?format=json";
        List<String> params=new ArrayList();
        ///params.add("");//id
        params.add(DeviceSession.getSingleton().getId());//device in param
        List<String> fields=history.getFields();
        List<HashMap> data=new ArrayList();
        HttpMethod method=new HttpGetMethod();
        method.setFields(fields);
        method.setParams(params);
        method.setUrl(url);/**/

        Message.getSingleton().setContext(MainActivity.ActivityContainer.getSingleton().getActivity().getApplicationContext());

        AsyncCallExecute async=new AsyncCallExecute();
        async.setMethod(method);

        async.setProcedure(getProcedure());
        async.setActivity(getActivity());
        async.execute();

    }

    public void getPrivateSensors(){

        String dataString="";
        List<HistoryInfo> sensors=new ArrayList();
        String url=HttpBaseURL.getBaseUrl()+"tokenapi/sensorstoken/?format=json";
        List<String> params=new ArrayList();
        //params.add("");//id
        params.add(DeviceSession.getSingleton().getId());//device in param
        List<String> fields=history.getFields();
        List<HashMap> data=new ArrayList();
        HttpMethod method=new HttpGetMethod();
        method.setFields(fields);
        method.setParams(params);
        method.setUrl(url);/**/

        Message.getSingleton().setContext(MainPrivateActivity.ActivityContainer.getSingleton().getActivity().getApplicationContext());

        AsyncCallExecute async=new AsyncCallExecute();
        async.setMethod(method);

        async.setProcedure(getProcedure());
        async.setActivity(getActivity());
        async.execute();

    }

	public List<SensorInfo> getAll(){
		List<SensorInfo> sensors=new ArrayList();
		String url=HttpBaseURL.getBaseUrl()+"webapi/sensors/?format=json";

		List<String> params=new ArrayList();
		List<String> fields=info.getFields();

		List<HashMap> data=new ArrayList();		

		HttpMethod method=new HttpGetMethod();
		method.setFields(fields);
		method.setParams(params);
		method.setUrl(url);

		AsyncCallExecute async=new AsyncCallExecute();
		async.setMethod(method);
		async.setActivity(getActivity());
		async.execute();

		String dataString=async.getData();
		try{
			data=JSONDecoderAll.decode(dataString,fields);
		}catch(Exception ex){}		

		for(HashMap d:data){
			SensorInfo sensor=new SensorInfo();
			sensor.setData(d);
			sensors.add(sensor);
		}
		return sensors;
	}

	public SensorInfo getById(String id){
		SensorInfo sensor=new SensorInfo();
		String url=HttpBaseURL.getBaseUrl()+"webapi/sensors/"+id+"/?format=json";

		List<String> params=new ArrayList();
		List<String> fields=info.getFields();

		HashMap data=new HashMap();		

		HttpMethod method=new HttpGetMethod();
		method.setFields(fields);
		method.setParams(params);
		method.setUrl(url);

		AsyncCallExecute async=new AsyncCallExecute();
		async.setMethod(method);
		async.setActivity(getActivity());
		async.execute();

		String dataString=async.getData();
		
		try{
			data=JSONDecoder.decode(dataString,fields);
		}catch(Exception ex){}		
		
		sensor.setData(data);

		return sensor;
	}
}
