package smartcity.loborobot.com.smartcityapp.callable;

import android.app.Activity;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import smartcity.loborobot.com.smartcityapp.R;
import smartcity.loborobot.com.smartcityapp.extra.SchedulersActivity;
import smartcity.loborobot.com.smartcityapp.info.SchedulerInfo;
import smartcity.loborobot.com.smartcityapp.json.JSONDecoderAll;
import smartcity.loborobot.com.smartcityapp.message.Message;
import smartcity.loborobot.com.smartcityapp.extra.schedulers.ArrayAdapterItem;
import smartcity.loborobot.com.smartcityapp.extra.schedulers.ObjectItem;
import smartcity.loborobot.com.smartcityapp.extra.schedulers.OnItemClickListenerListViewItem;

/**
 * Created by fincyt on 13/01/15.
 */

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */
public class PrivateActivitySchedulersProcedure implements Procedure{
    /**
     * data in text to decode
     * */
    private Activity main;
    public void setActivity(Activity a){
        main=a;
    }
    public Activity getActivity(){
        return main;
    }
    private String data;
    public void setData(String d){
        data=d;
    }
    public String getData(){
        return data;
    }
    private List<String> fields=new ArrayList();
    public void setFields(List<String> fs){
        for(String s:fs){
            fields.add(s);
        }
    }
    public List<String> getFields(){
        return fields;
    }
    private List objects=new ArrayList();
    public void setObjects(List os){
        for(Object o:os){
            objects.add(o);
        }
    }
    public List getObjects(){
        return objects;
    }
    public void execute(){

        Message.getSingleton().setContext(getActivity().getApplicationContext());
//
        String dataString=getData();
//
        List<ObjectItem> sdata=new ArrayList();
//
        List<HashMap> data=new ArrayList();
        try{
            data= JSONDecoderAll.decode(dataString, getFields());
        }catch(Exception ex){}


//
        for(HashMap d:data){
            SchedulerInfo object=new SchedulerInfo();
            object.setData(d);
            String id="";
            try{
                id=object.getId();
                id=id.trim();
            }catch(Exception ex){}
            String title="";
            try{
                title=object.getName();
                title=title.trim();
            }catch(Exception ex){}
            String hour="";
            try{
                hour=object.getTimeBegin();
                hour=hour.trim();
            }catch(Exception ex){}

            String enabled="";
            try{
                enabled=object.getEnabled();
            }catch(Exception ex){}

            Boolean flag=false;
            try{
                flag=Boolean.parseBoolean(enabled);
            }catch(Exception ex){}

            HashMap info=new HashMap();
            info.put("id", id);
            info.put("name", title);
            info.put("enable",enabled);
            info.put("hour",hour);

            ObjectItem sd=new ObjectItem(info);
            sdata.add(sd);

            objects.add(object);
        }
        ObjectItem[] ObjectItemData = new ObjectItem[sdata.size()];
        int cont=0;
        for(ObjectItem dt:sdata){
            ObjectItemData[cont]=new ObjectItem(dt.data);
            cont++;
        }
        ArrayAdapterItem adapter = new ArrayAdapterItem(SchedulersActivity.ObjectSingleton.getSingleton().getSchedulersActivity(), R.layout.extra_row_list_item, ObjectItemData);
        ListView listViewItems = (ListView) SchedulersActivity.ObjectSingleton.getSingleton().getSchedulersActivity().findViewById(R.id.listViewSchedulers);
        listViewItems.setAdapter(adapter);
        listViewItems.setOnItemClickListener(new OnItemClickListenerListViewItem());
    }
}