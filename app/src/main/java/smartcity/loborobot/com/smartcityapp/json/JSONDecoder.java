package smartcity.loborobot.com.smartcityapp.json;

import java.util.*;

import org.json.*;

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */

public class JSONDecoder {

	public static HashMap decode(String dataString,List< String > fields){
		HashMap data=new HashMap();

		JSONObject object=null;
		String val;
		try{
		 	object=new JSONObject(dataString);
		}catch(Exception ex){}
		if(object!=null){
			try{				
				for(String f : fields){
					val="";
					try{
						val=object.get(f).toString();
					}catch(Exception ex){
						val="";
					}
					if(val!=""){							
						data.put(f,val);							
					}						
				}
		 	}catch(Exception ex){}
		}

		return data;
	}

}