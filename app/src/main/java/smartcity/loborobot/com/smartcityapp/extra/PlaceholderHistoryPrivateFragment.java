package smartcity.loborobot.com.smartcityapp.extra;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.widget.TextView;

import java.util.List;

import smartcity.loborobot.com.smartcityapp.MainActivity;
import smartcity.loborobot.com.smartcityapp.R;
import smartcity.loborobot.com.smartcityapp.callable.PrivateDeviceSensorsProcedure;
import smartcity.loborobot.com.smartcityapp.info.HistoryInfo;
import smartcity.loborobot.com.smartcityapp.message.Message;

/**
 * Created by fincyt on 09/01/15.
 */
/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */
public class PlaceholderHistoryPrivateFragment extends Fragment {

    private LayoutInflater inflater;
    public void setLayoutInflater(LayoutInflater l){
        inflater=l;
    }
    public LayoutInflater getLayoutInflater(){
        return inflater;
    }
    private ViewGroup container;
    public void setViewGroup(ViewGroup v){
        container=v;
    }
    public ViewGroup getViewGroup(){
        return container;
    }
    private Bundle savedInstanceState;

    public void setBundle(Bundle b){
        savedInstanceState=b;
    }
    public Bundle getBundle(){
        return savedInstanceState;
    }

    private static MainPrivateActivity activity;

    public static void setMainPrivateActivity(MainPrivateActivity a){
        activity=a;
    }
    public static MainPrivateActivity getMainPrivateActivity(){
        return activity;
    }

    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static PlaceholderHistoryPrivateFragment newInstance(int sectionNumber) {
        PlaceholderHistoryPrivateFragment fragment = new PlaceholderHistoryPrivateFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public PlaceholderHistoryPrivateFragment() {

    }
    public static class SensorIndex{
        private static SensorIndex singleton;
        private static int index;
        private SensorIndex(){
            index=0;
        }
        public static SensorIndex getSingleton(){
            if(singleton==null){
                singleton=new SensorIndex();
            }
            return singleton;
        }
        public static void setIndex(int i){
            index=i;
        }
        public static int getIndex(){
            return index;
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setLayoutInflater(inflater);
        setViewGroup(container);
        setBundle(savedInstanceState);

        View rootView = inflater.inflate(R.layout.extra_fragment_history, container, false);

        int index = SensorIndex.getSingleton().getIndex();

        //Message.getSingleton().setContext(MainPrivateActivity.ActivityContainer.getSingleton().getActivity().getApplicationContext());
        //Message.getSingleton().show(" : "+index);

        List<HistoryInfo> list_histories =null;
        try{
            list_histories=PrivateDeviceSensorsProcedure.HistoryInfoContainer.getSingleton().getData();
        }catch(Exception ex){}

        HistoryInfo data=null;
        try{
            data=list_histories.get(index);
        }catch(Exception ex){}

        String sensors[]={"temperature","humidity","sound","flowmeter","light","nitrogen_dioxide","carbon_monoxide"};
        String measures[]={"C°","%RH","DB","L/H","LUX","PBM","PPM"};
        if(data!=null) {
            TextView texto = null;
            try{
                texto=(TextView) rootView.findViewById(R.id.textViewHistory);
            }catch(Exception ex){}
            String label="";
            String measure="";
            String value="";
            try{
                label=sensors[index].toUpperCase();
            }catch(Exception ex){
                label="";
            }
            try{
                measure=measures[index].toUpperCase();
            }catch(Exception ex){
                measure="";
            }
            try{
                value=data.getValue();
            }catch(Exception ex){
                value="";
            }
            try{
                texto.setText(label+" : "+value+" "+measure);
            }catch(Exception ex){}

        }

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainPrivateActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }
}
