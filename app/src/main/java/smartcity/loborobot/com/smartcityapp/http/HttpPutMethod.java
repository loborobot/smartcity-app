package smartcity.loborobot.com.smartcityapp.http;

import java.util.*;

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */

public class HttpPutMethod implements HttpMethod{

	protected List<HashMap> params=new ArrayList();
	protected List<String> 	fields=new ArrayList();
	protected String url="";
	protected String method="";	
	protected String answer="";	

	public HttpPutMethod(){
		super();	
		method="put";
	}
	public void setParams(List<String> ps){//setParams
		String p="";
		int cont=0;
		for(String f : fields){		
			p="";
			p=ps.get(cont);
			HashMap d=new HashMap();
			d.put("field",f);
			d.put("data",p);
			cont++;
			params.add(d);
		}
	}
	public void setFields(List<String> fs){//fields
		for(String f : fs){
			fields.add(f);
		}
	}
    public List<String> getFields(){
        return fields;
    }
	public void setUrl(String u){//url
		url=u;
	}
	public String getUrl(){//url
		return url;
	}
	public void send(){
		answer=HttpPutJsonAction.send(params,url);
	}
	public void execute(){
		send();
	}
	public String getAnswer(){
		return answer;
	}
}
