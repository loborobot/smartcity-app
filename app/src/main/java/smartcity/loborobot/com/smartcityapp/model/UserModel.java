
package smartcity.loborobot.com.smartcityapp.model;

import java.util.*;

import smartcity.loborobot.com.smartcityapp.MainActivity;
import smartcity.loborobot.com.smartcityapp.callable.Procedure;
import smartcity.loborobot.com.smartcityapp.http.*;

import smartcity.loborobot.com.smartcityapp.info.*;

import smartcity.loborobot.com.smartcityapp.message.Message;
import smartcity.loborobot.com.smartcityapp.model.*;

import smartcity.loborobot.com.smartcityapp.async.*;

import smartcity.loborobot.com.smartcityapp.json.*;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import org.json.*;

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */

public class UserModel{

	private Activity main;

	private UserInfo info;

    private Procedure procedure;

    public void setProcedure(Procedure p){
        procedure=p;
    }
    public Procedure getProcedure() {
        return procedure;
    }
	
	public void setActivity(Activity a){
		main=a;
	}
	public Activity getActivity(){
		return main;
	}

	public UserModel(){
		info=new UserInfo();
	}

	public void Login(String user,String pass){
        Message.getSingleton().setContext(MainActivity.ActivityContainer.getSingleton().getActivity().getApplicationContext());

		boolean answer=false;
		String url=HttpBaseURL.getBaseUrl()+"tokenapi/login/?format=json";

		List<String> params=new ArrayList();
		params.add(user);
		params.add(pass);
		List<String> fields=new ArrayList();

		fields.add("user");
		fields.add("password");

        //Message.getSingleton().show("data: "+user+", "+pass);

		//HashMap data=new HashMap();

		HttpMethod method=new HttpPostMethod();
		method.setFields(fields);
		method.setParams(params);
		method.setUrl(url);

        AsyncCallExecute async=new AsyncCallExecute();
        async.setMethod(method);
        async.setProcedure(getProcedure());//esto va despues del metodo siempre
        async.setActivity(getActivity());
        async.execute();


	}

	public HashMap UserLogin(String user,String pass){
		HashMap data=new HashMap();
		String url=HttpBaseURL.getBaseUrl()+"privateapi/login/?format=json";

		List<String> params=new ArrayList();
		params.add(user);
		params.add(pass);
		List<String> fields=new ArrayList();

		fields.add("user");
		fields.add("password");

		HttpMethod method=new HttpPostMethod();
		method.setFields(fields);
		method.setParams(params);
		method.setUrl(url);

		AsyncCallExecute async=new AsyncCallExecute();
		async.setMethod(method);
		async.setActivity(getActivity());
		async.execute();

		String dataString=async.getData();
		
		try{
			data=JSONDecoder.decode(dataString,fields);
		}catch(Exception ex){}		

		return data;
	}

	public boolean Session(String token){
		boolean answer=true;
		String url=HttpBaseURL.getBaseUrl()+"privateapi/session/?format=json";

		List<String> params=new ArrayList();
		params.add(token);

		List<String> fields=new ArrayList();

		fields.add("token");

		HashMap data=new HashMap();		

		HttpMethod method=new HttpPostMethod();
		method.setFields(fields);
		method.setParams(params);
		method.setUrl(url);

		AsyncCallExecute async=new AsyncCallExecute();
		async.setMethod(method);
		async.setActivity(getActivity());
		async.execute();

		String dataString=async.getData();
		
		try{
			data=JSONDecoder.decode(dataString,fields);
		}catch(Exception ex){}		
		
		if(data.get("success").toString().compareTo("true")==0){
			answer=true;
		}else{
			answer=false;
		}


		return answer;
	}	


	public boolean closeSession(String token){
		boolean answer=true;
		String url=HttpBaseURL.getBaseUrl()+"privateapi/closesession/?format=json";


		List<String> params=new ArrayList();
		params.add(token);

		List<String> fields=new ArrayList();

		fields.add("token");

		HashMap data=new HashMap();		

		HttpMethod method=new HttpPostMethod();
		method.setFields(fields);
		method.setParams(params);
		method.setUrl(url);

		AsyncCallExecute async=new AsyncCallExecute();
		async.setMethod(method);
		async.setActivity(getActivity());
		async.execute();

		String dataString=async.getData();
		
		try{
			data=JSONDecoder.decode(dataString,fields);
		}catch(Exception ex){}		
		
		if(data.get("success").toString().compareTo("true")==0){
			answer=true;
		}else{
			answer=false;
		}

		return answer;
	}	
}