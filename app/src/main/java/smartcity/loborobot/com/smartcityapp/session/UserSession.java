package smartcity.loborobot.com.smartcityapp.session;

/**
 * Created by fincyt on 15/01/15.
 */

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */

 public class UserSession {
    private static  UserSession singleton;
    private UserSession(){

    }
    public static UserSession getSingleton(){
        if(singleton==null){
            singleton=new UserSession();
        }
        return singleton;
    }

    private static String username;
    private static String password;
    private static String token;
    private static String success;

    public static String getUsername() {
        return username;
    }

    public static void setUsername(String username) {
        UserSession.username = username;
    }

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String password) {
        UserSession.password = password;
    }

    public static String getToken() {
        return token;
    }

    public static void setToken(String token) {
        UserSession.token = token;
    }

    public static String getSuccess() {
        return success;
    }

    public static void setSuccess(String success) {
        UserSession.success = success;
    }



}
