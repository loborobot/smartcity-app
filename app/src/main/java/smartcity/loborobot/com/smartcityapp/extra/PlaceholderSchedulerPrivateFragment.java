package smartcity.loborobot.com.smartcityapp.extra;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import smartcity.loborobot.com.smartcityapp.R;
import smartcity.loborobot.com.smartcityapp.extra.data.SchedulerAdapter;
import smartcity.loborobot.com.smartcityapp.extra.data.SchedulerData;

/**
 * Created by fincyt on 09/01/15.
 */
/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */
public class PlaceholderSchedulerPrivateFragment extends Fragment {

    private LayoutInflater inflater;
    public void setLayoutInflater(LayoutInflater l){
        inflater=l;
    }
    public LayoutInflater getLayoutInflater(){
        return inflater;
    }
    private ViewGroup container;
    public void setViewGroup(ViewGroup v){
        container=v;
    }
    public ViewGroup getViewGroup(){
        return container;
    }
    private Bundle savedInstanceState;

    public void setBundle(Bundle b){
        savedInstanceState=b;
    }
    public Bundle getBundle(){
        return savedInstanceState;
    }

    private ListView listViewSchedulers;

    private static MainPrivateActivity activity;

    public static void setMainPrivateActivity(MainPrivateActivity a){
        activity=a;
    }
    public static MainPrivateActivity getMainPrivateActivity(){
        return activity;
    }

    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static PlaceholderSchedulerPrivateFragment newInstance(int sectionNumber) {
        PlaceholderSchedulerPrivateFragment fragment = new PlaceholderSchedulerPrivateFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public PlaceholderSchedulerPrivateFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setLayoutInflater(inflater);
        setViewGroup(container);
        setBundle(savedInstanceState);

        View rootView = inflater.inflate(R.layout.extra_fragment_scheduler, container, false);
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainPrivateActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }
}
