package smartcity.loborobot.com.smartcityapp;


import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.concurrent.Callable;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */

 public class MapPublicDrawerThread extends  Thread implements  Runnable{

    private GoogleMap map;
    private LatLng AQP;

    public void setMap(GoogleMap m){
        map=m;
    }
    public GoogleMap getMap(){
        return map;
    }



    @Override
    public void run(){


        AQP=new LatLng(-16.3992433, -71.5369622);
        if(getMap()!=null){
            try {
                getMap().setMyLocationEnabled(true);
            }catch(Exception ex){}
            try {
                getMap().addMarker(new MarkerOptions().position(AQP).title("Arequipa"));
            }catch(Exception ex){}
            try{
                //getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(AQP, 14));
            }catch(Exception ex){}
            try{
                //getMap().animateCamera(CameraUpdateFactory.zoomBy(18));
            }catch(Exception ex){}
            try{
                //getMap().moveCamera(CameraUpdateFactory.newLatLng(AQP));
            }catch (Exception ex){}
        }

    }

}
