package smartcity.loborobot.com.smartcityapp.info;

import java.util.*;

import smartcity.loborobot.com.smartcityapp.info.*;

import smartcity.loborobot.com.smartcityapp.http.*;

import smartcity.loborobot.com.smartcityapp.async.*;

import smartcity.loborobot.com.smartcityapp.json.*;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import org.json.*;

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */

public class SchedulerInfo{

    private String token; //token
	private String device; //device  id
    private String name; //name of device
	private String time_begin; // time for begin
	private String time_end; //time for end
	private String date_begin; //date for begin schedule action
	private String date_end; //date for end schedule action
	private String frecuency; //frecuency for schedule action
	private String rule;      //rule in schdedule
	private String enabled;		// enable action for scheduler
	private String valve;		//number of valve active in a moment

	private List<String> fields;
    private String id;

    public SchedulerInfo(){
		super();
		fields=new ArrayList();
        fields.add("token");
        fields.add("id");
		fields.add("device");
        fields.add("name");
		fields.add("time_begin");
		fields.add("time_end");
		fields.add("date_begin");
		fields.add("date_end");
		fields.add("frecuency");
		fields.add("rule");
		fields.add("enabled");
		fields.add("valve");
        fields.add("success");
	}
	public List<String> toData(){
		List<String> data=new ArrayList();

		HashMap d=toHashMap();
		String s="";
		for(String f: fields){
			try{
				s=d.get(f).toString();
			}catch(Exception ex){
				s="";
			}	
			data.add(s);
		}

		return data;
	}

    public String getToken() {
        return token;
    }

    public HashMap toHashMap(){
		HashMap data=new HashMap();
		String val="";
		for(String f: fields){
			val="";
            if(f.compareTo("token")==0){
                try{
                    val=getToken();
                }catch(Exception ex){
                    val="";
                }
            }
            if(f.compareTo("id")==0){
                try{
                    val=getId();
                }catch(Exception ex){
                    val="";
                }
            }
			if(f.compareTo("device")==0){
				try{
					val=getDevice();
				}catch(Exception ex){
					val="";
				}
			}
            if(f.compareTo("name")==0){
                try{
                    val=getName();
                }catch(Exception ex){
                    val="";
                }
            }
			if(f.compareTo("time_begin")==0){
				try{
					val=getTimeBegin();
				}catch(Exception ex){
					val="";
				}
			}
			if(f.compareTo("time_end")==0){
				try{
					val=getTimeEnd();
				}catch(Exception ex){
					val="";
				}
			}
			if(f.compareTo("date_begin")==0){
				try{
					val=getDateBegin();
				}catch(Exception ex){
					val="";
				}
			}
			if(f.compareTo("date_end")==0){
				try{
					val=getDateEnd();
				}catch(Exception ex){
					val="";
				}
			}
			if(f.compareTo("frecuency")==0){
				try{
					val=getFrecuency();
				}catch(Exception ex){
					val="";
				}
			}
			if(f.compareTo("rule")==0){
				try{
					val=getRule();
				}catch(Exception ex){
					val="";
				}
			}
			if(f.compareTo("enabled")==0){
				try{
					val=getEnabled();
				}catch(Exception ex){
					val="";
				}
			}
			if(f.compareTo("valve")==0){
				try{
					val=getValve();
				}catch(Exception ex){
					val="";
				}
			}
			if(val.compareTo("")!=0){
				data.put(f,val);	
			}			
		}

		return data;
	}

    public void setData(HashMap data) {
        String val = "";
        for (String f : fields) {
            val = "";
            try {
                val = data.get(f).toString();
            } catch (Exception ex) {
                val = "";
            }
            if (f.compareTo("id") == 0) {
                setId(val);
            }
            if (f.compareTo("token") == 0) {
                setToken(val);
            }
            if (f.compareTo("device") == 0) {
                setDevice(val);
            }
            if (f.compareTo("name") == 0) {
                setName(val);
            }
            if (f.compareTo("time_begin") == 0) {
                setTimeBegin(val);
            }
            if (f.compareTo("time_end") == 0) {
                setTimeEnd(val);
            }
            if (f.compareTo("date_begin") == 0) {
                setDateBegin(val);
            }
            if (f.compareTo("date_end") == 0) {
                setDateEnd(val);
            }
            if (f.compareTo("frecuency") == 0) {
                setFrecuency(val);
            }
            if (f.compareTo("rule") == 0) {
                setRule(val);
            }
            if (f.compareTo("enabled") == 0) {
                setEnabled(val);
            }
            if (f.compareTo("valve") == 0) {
                setValve(val);
            }
        }
    }

	public List<String> getFields(){
		return fields;
	}
	public String getDevice(){
		return device;
	}
	public void setDevice(String n){
		device=n;
	}

	public String getTimeBegin(){
		return time_begin;
	}
	public void setTimeBegin(String n){
		time_begin=n;
	}

	public String getTimeEnd(){
		return time_end;
	}
	public void setTimeEnd(String n){
		time_end=n;
	}

	public String getDateBegin(){
		return date_begin;
	}
	public void setDateBegin(String n){
		date_begin=n;
	}

	public String getDateEnd(){
		return date_end;
	}
	public void setDateEnd(String n){
		date_end=n;
	}

	public String getFrecuency(){
		return frecuency;
	}
	public void setFrecuency(String n){
		frecuency=n;
	}

	public String getRule(){
		return rule;
	}
	public void setRule(String n){
		rule=n;
	}
	public String getEnabled(){
		return enabled;
	}
	public void setEnabled(String n){
		enabled=n;
	}	
	public String getValve(){
		return valve;
	}
	public void setValve(String n){
		valve=n;
	}

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setId(String i) {
        this.id= i;
    }

    public String getId() {
        return id;
    }
}
