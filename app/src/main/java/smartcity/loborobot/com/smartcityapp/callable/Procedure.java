package smartcity.loborobot.com.smartcityapp.callable;

import android.app.Activity;

import java.util.List;

/**
 * Created by fincyt on 13/01/15.
 */

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */

 public interface Procedure {
    public void setActivity(Activity a);
    public Activity getActivity();
    public void setObjects(List o);
    public List getObjects();
    public void setData(String d);
    public String getData();
    public void setFields(List<String> l);
    public List<String> getFields();
    public void execute();
}
