package smartcity.loborobot.com.smartcityapp.http;

import java.util.*;

import smartcity.loborobot.com.smartcityapp.MainActivity;
import smartcity.loborobot.com.smartcityapp.message.Message;

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */

public class HttpPostMethod implements HttpMethod{

	protected List params=new ArrayList();
	protected List fields=new ArrayList();
	protected String url="";
	protected String method="";	
	protected String answer="";

	public HttpPostMethod(){
		super();	
		method="post";
	}
	public void setParams(List<String> ps){//params

        //Message.getSingleton().setContext(MainActivity.ActivityContainer.getSingleton().getActivity().getApplicationContext());

		String p="";
		int cont=0;
		for(Object f : fields){		
			p="";
            try{
                p=ps.get(cont);
            }catch(Exception ex){
                p="";
            }
			HashMap d=new HashMap();
			d.put("field",f.toString());
			d.put("data",p);
			cont++;
			params.add(d);
		}

        //Message.getSingleton().show("params: "+params.toString());
	}
	public void setFields(List<String> fs){//fields
		for(String f : fs){
			fields.add(f);
		}
	}
    public List<String> getFields(){
        return fields;
    }
	public void setUrl(String u){//url
		url=u;
	}
	public String getUrl(){//url
		return url;
	}
	public void send(){
		answer=HttpPostAction.send(params,url);
	}
	public void execute(){
		send();
	}
	public String getAnswer(){
		return answer;
	}
}
