package smartcity.loborobot.com.smartcityapp.callable;

import android.app.Activity;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import smartcity.loborobot.com.smartcityapp.R;
import smartcity.loborobot.com.smartcityapp.extra.SchedulersActivity;
import smartcity.loborobot.com.smartcityapp.extra.schedulers.ArrayAdapterItem;
import smartcity.loborobot.com.smartcityapp.extra.schedulers.ObjectItem;
import smartcity.loborobot.com.smartcityapp.extra.schedulers.OnItemClickListenerListViewItem;
import smartcity.loborobot.com.smartcityapp.info.SchedulerInfo;
import smartcity.loborobot.com.smartcityapp.json.JSONDecoder;
import smartcity.loborobot.com.smartcityapp.json.JSONDecoderAll;
import smartcity.loborobot.com.smartcityapp.message.Message;

/**
 * Created by fincyt on 03/04/15.
 */

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */
public class PrivateUpdateSchedulersProcedure implements Procedure{
    /**
     * data in text to decode
     * */
    private Activity main;
    public void setActivity(Activity a){
        main=a;
    }
    public Activity getActivity(){
        return main;
    }
    private String data;
    public void setData(String d){
        data=d;
    }
    public String getData(){
        return data;
    }
    private List<String> fields=new ArrayList();
    public void setFields(List<String> fs){
        for(String s:fs){
            fields.add(s);
        }
    }
    public List<String> getFields(){
        return fields;
    }
    private List objects=new ArrayList();
    public void setObjects(List os){
        for(Object o:os){
            objects.add(o);
        }
    }
    public List getObjects(){
        return objects;
    }
    public void execute(){

        Message.getSingleton().setContext(getActivity().getApplicationContext());
        Logger log= Logger.getLogger("debug");
        String dataString=getData();
        HashMap data=new HashMap<String,String>();
        try{
            data= JSONDecoder.decode(dataString, getFields());
        }catch(Exception ex){}
        if(data.get("success").toString().compareTo("true")==0){
            Message.getSingleton().show("Data Guardada!");
            log.info("Data guardada en scheduler");
        }


    }
}