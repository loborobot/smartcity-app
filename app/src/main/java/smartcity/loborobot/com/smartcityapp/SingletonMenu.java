package smartcity.loborobot.com.smartcityapp;

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */


 public class SingletonMenu {

    private static SingletonMenu singleton;
    private static  int option;

    public static int getOption(){
        return option;
    }
    public static void setOption(int o){
        option=o;
    }
    public static SingletonMenu getSingletonMenu(){
        if(singleton==null){
            singleton=new SingletonMenu();
        }
        return singleton;
    }
    private SingletonMenu(){
        option=2;
    }
}
