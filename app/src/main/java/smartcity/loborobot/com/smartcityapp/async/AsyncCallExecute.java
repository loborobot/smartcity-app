package smartcity.loborobot.com.smartcityapp.async;

/**
 * Created by fincyt on 13/01/15.
 */

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */

 import java.util.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
 import java.util.logging.Logger;

 import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import smartcity.loborobot.com.smartcityapp.callable.Procedure;
import smartcity.loborobot.com.smartcityapp.http.*;

import smartcity.loborobot.com.smartcityapp.info.*;

 import smartcity.loborobot.com.smartcityapp.message.Message;
 import smartcity.loborobot.com.smartcityapp.model.*;

public class AsyncCallExecute extends AsyncTask<String, Void, String>{
    private Activity main;
    private Boolean released;
    private HttpMethod method;
    private String data="";
    private Procedure procedure;
    private List<String> fields;
    public AsyncCallExecute(){
        super();
        try{
            main=null;
        }catch(Exception ex){}
        try{
            released=false;
        }catch(Exception ex){}

    }
    public void setFields(List<String > fs){
        fields=new ArrayList();
        for(String f:fs){
            try{
                fields.add(f);
            }catch(Exception ex){}

        }
    }
    public List<String> getFields(){
        return fields;
    }
    public void setProcedure(Procedure proc){
        procedure=proc;
    }
    public Procedure getProcedure(){
        return procedure;
    }
    public void setActivity(Activity a){
        main=a;
    }
    public Activity getActivity(){
        return main;
    }
    public void setMethod(HttpMethod m){
        try{
            method=m;
        }catch(Exception ex){}
        try{
            setFields(m.getFields());
        }catch(Exception ex){}

    }
    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        released=true;

        try{
            Message.getSingleton().setContext(getActivity().getApplicationContext());
        }catch(Exception ex){}

        Logger log= Logger.getLogger("debug");

        if(result!=null) {
            //Toast.makeText(main.getApplicationContext(), result, Toast.LENGTH_LONG).show();
            if (result.equals("")) {
                Toast.makeText(main.getApplicationContext(), "No result", Toast.LENGTH_LONG).show();
            } else {
                //Toast.makeText(main.getApplicationContext(), result, Toast.LENGTH_LONG).show();
                try {
                    getProcedure().setActivity(getActivity());
//                    Message.getSingleton().show(getActivity().toString());

                }catch(Exception ex){}
                try {
                    getProcedure().setData(result);

//
//                    Message.getSingleton().show(result);

                    log.info(result);

                }catch(Exception ex){}
                try{
                    getProcedure().setFields(getFields());
                }catch(Exception ex){}
                try{
                    getProcedure().execute();
                }catch(Exception ex){}
            }
        }else{
            try{
                Toast.makeText(main.getApplicationContext(), "null result", Toast.LENGTH_LONG).show();
            }catch(Exception ex){}

        }
    }
    @Override
    protected String doInBackground(String... params) {
        // TODO Auto-generated method stub
        String answer="";

        if(method!=null){
            method.execute();
        }
        if(method!=null){
            answer=method.getAnswer();
        }else{
            answer="";
        }
        data=answer;

        return answer;
    }
    public String getData(){
        return data;
    }
    public Boolean getReleased(){
        return released;
    }

}
