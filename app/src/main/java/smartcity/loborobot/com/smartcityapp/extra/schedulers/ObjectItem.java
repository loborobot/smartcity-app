package smartcity.loborobot.com.smartcityapp.extra.schedulers;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by guest on 02/04/15.
 */
public class ObjectItem {

    public HashMap data;

    public String getValue(String field){
        String value="";
        if(data!=null){
            try{
                value= (String) data.get(field);
            }catch(Exception ex){}
        }
        return value;
    }
    public void setValue(String field,String value){
        if(data==null){
            data=new HashMap();
        }
        data.put(field,value);
    }
    public ObjectItem(HashMap d) {
        data=new HashMap();
        Iterator it = d.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            String key=pair.getKey().toString();
            String value=pair.getValue().toString();
            data.put(key,value);
            it.remove();
        }
    }
}
