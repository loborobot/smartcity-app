package smartcity.loborobot.com.smartcityapp.info;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */

public class HistoryInfo {
    private String id; //id de history for sensor
	private String device; //device in history for sensor
	private String sensor; //sensor in history for sensor
	private String date; //date in history for sensor
    private String value; //value in history for sensor

	private List<String> fields;

	public HistoryInfo(){
		super();
		fields=new ArrayList();
		fields.add("id");
		fields.add("device");
		fields.add("sensor");
		fields.add("date");
        fields.add("value");
	}

	public List<String> toData(){
		List<String> data=new ArrayList();

		HashMap d=toHashMap();
		String s="";
		for(String f: fields){
			try{
				s=d.get(f).toString();
			}catch(Exception ex){
				s="";
			}	
			data.add(s);
		}

		return data;
	}



    public HashMap toHashMap(){
		HashMap data=new HashMap();
		String val="";
		for(String f: fields){
			val="";			
			if(f.compareTo("id")==0){
				try{
					val=getId();
				}catch(Exception ex){
					val="";
				}
			}
			if(f.compareTo("device")==0){
				try{
					val=getDevice();
				}catch(Exception ex){
					val="";
				}
			}
			if(f.compareTo("sensor")==0){
				try{
					val=getSensor();
				}catch(Exception ex){
					val="";
				}
			}
			if(f.compareTo("date")==0){
				try{
					val=getDate();
				}catch(Exception ex){
					val="";
				}
			}
            if(f.compareTo("value")==0){
                try{
                    val=getValue();
                }catch(Exception ex){
                    val="";
                }
            }
			if(val.compareTo("")!=0){
				data.put(f,val);	
			}			
		}

		return data;
	}

	public void setData(HashMap data){
		String val="";
		for(String f: fields){
			val="";
			try{
				val=data.get(f).toString();
			}catch(Exception ex){
				val="";
			}
			if(f.compareTo("id")==0){
				setId(val);
			}
			if(f.compareTo("device")==0){
				setDevice(val);
			}
			if(f.compareTo("sensor")==0){
				setSensor(val);
			}
			if(f.compareTo("date")==0){
				setDate(val);
			}
            if(f.compareTo("value")==0){
                setValue(val);
            }
		}
	}
	public List<String> getFields(){
		return fields;
	}

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getSensor() {
        return sensor;
    }

    public void setSensor(String sensor) {
        this.sensor = sensor;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
