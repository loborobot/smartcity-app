package smartcity.loborobot.com.smartcityapp.info;

import java.util.*;

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */

public class SensorInfo{
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String id; //name of sensor
	private String name; //name of sensor
	private String unity; //unity for sensor
	private String minimum; //minimum value of sensoring
	private String maximum; //maximum value of sensoring
	private List<String> fields;

	public SensorInfo(){
		super();
		fields=new ArrayList();
        fields.add("id");
		fields.add("name");
		fields.add("unity");
		fields.add("minimum_range");
		fields.add("maximum_range");
	}

	public List<String> toData(){
		List<String> data=new ArrayList();

		HashMap d=toHashMap();
		String s="";
		for(String f: fields){
			try{
				s=d.get(f).toString();
			}catch(Exception ex){
				s="";
			}	
			data.add(s);
		}

		return data;
	}
	public HashMap toHashMap(){
		HashMap data=new HashMap();
		String val="";
		for(String f: fields){
			val="";
            if(f.compareTo("id")==0){
                try{
                    val=getId();
                }catch(Exception ex){
                    val="";
                }
            }
			if(f.compareTo("name")==0){
				try{
					val=getName();
				}catch(Exception ex){
					val="";
				}
			}
			if(f.compareTo("unity")==0){
				try{
					val=getUnity();
				}catch(Exception ex){
					val="";
				}
			}
			if(f.compareTo("minimum")==0){
				try{
					val=getMinimum();
				}catch(Exception ex){
					val="";
				}
			}
			if(f.compareTo("maximum")==0){
				try{
					val=getMaximum();
				}catch(Exception ex){
					val="";
				}
			}
			if(val.compareTo("")!=0){
				data.put(f,val);	
			}			
		}

		return data;
	}

	public void setData(HashMap data){
		String val="";
		for(String f: fields){
			val="";
			try{
				val=data.get(f).toString();
			}catch(Exception ex){
				val="";
			}
            if(f.compareTo("id")==0){
                setId(val);
            }
			if(f.compareTo("name")==0){
				setName(val);
			}
			if(f.compareTo("unity")==0){
				setUnity(val);
			}
			if(f.compareTo("minimum")==0){
				setMinimum(val);
			}
			if(f.compareTo("maximum")==0){
				setMaximum(val);
			}
		}
	}
	public List<String> getFields(){
		return fields;
	}
	public String getName(){
		return name;
	}
	public void setName(String n){
		name=n;
	}

	public String getUnity(){
		return unity;
	}
	public void setUnity(String n){
		unity=n;
	}

	public String getMinimum(){
		return minimum;
	}
	public void setMinimum(String n){
		minimum=n;
	}

	public String getMaximum(){
		return maximum;
	}
	public void setMaximum(String n){
		maximum=n;
	}
}
