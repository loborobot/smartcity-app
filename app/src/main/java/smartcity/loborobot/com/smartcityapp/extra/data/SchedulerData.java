package smartcity.loborobot.com.smartcityapp.extra.data;

/**
 * Created by Christian Portilla Pauca on 20/12/14.
 */

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */

public class SchedulerData {
    public String   title;
    public Boolean  enable;
    public String   hour;

    public String getTitle(){
        return title;
    }
    public void setTitle(String t){
        title=t;
    }
    public String getHour(){
        return hour;
    }
    public void setHour(String t){
        hour=t;
    }
    public Boolean getEnable(){
        return enable;
    }
    public void setEnable(Boolean e){
        enable=e;
    }

    public SchedulerData(){
        super();
        title="";
        enable=false;
        hour="";
    }

    public SchedulerData(String title, Boolean enable){
        super();
        this.title=title;
        this.enable=enable;
        this.hour="";
    }
    public SchedulerData(String title, Boolean enable,String hour){
        super();
        this.title=title;
        this.enable=enable;
        this.hour=hour;
    }
}
