package smartcity.loborobot.com.smartcityapp.extra.maps;

import com.google.android.gms.maps.model.Marker;

/**
 * Created by fincyt on 23/01/15.
 */
/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */

 public class MarkerInfo {


    private Marker marker=null;
    private String id="";

    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }
    public void setId(String id){
        this.id=id;
    }
    public String getId(){
        return this.id;
    }

}
