package smartcity.loborobot.com.smartcityapp.database;

import android.content.Context;
import android.database.*;
import android.database.sqlite.*;
import android.util.Log;

import java.io.File;
import java.util.HashMap;

import smartcity.loborobot.com.smartcityapp.MainActivity;
import smartcity.loborobot.com.smartcityapp.message.Message;

/**
 * Created by fincyt on 15/01/15.
 */


/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */

public class Database extends SQLiteOpenHelper{
    private static  Database singleton;
    private static String DatabaseName="smartcitydb.db";
    private static final int DATABASE_VERSION = 1;
    public Database(){
        super(MainActivity.ActivityContainer.getSingleton().getActivity().getApplicationContext(), DatabaseName, null, DATABASE_VERSION);
    }
    public static Database getSingleton(){
        if(singleton==null){
            singleton=new Database();
        }
        return singleton;
    }
    public static SQLiteDatabase getDB(){
        return MainActivity.BDContainer.getSingleton().getDb();
    }
    public static void createTableUser(){
        getDB().execSQL(
                "CREATE TABLE  IF NOT EXISTS user "
                + " ( " +
                  "id INT PRIMARY KEY NOT NULL,"+
                  " user CHAR(200)," +
                  " password char(200)," +
                  " success char(20)," +
                  " token char(200)" +
                  ");"
        );
    }
    public static void insertUserDummy(){
        String sql="INSERT INTO user VALUES (1,\"dummy\",\"dummy\",\"false\",\"dummy\");";
        try {
            getDB().execSQL(sql);
        }catch(Exception ex){}
    }

    public static void updateUser(String user,String password,String success,String token){
        String sql="update  user set user='"+user+"' , password='"+password+"',success='"+success+"' , token='"+token+"' where id = 1 ";
        try {
            getDB().execSQL(sql);
        }catch(Exception ex){}
    }



    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {

    }


}
