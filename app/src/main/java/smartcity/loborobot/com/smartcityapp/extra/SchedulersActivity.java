package smartcity.loborobot.com.smartcityapp.extra;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import smartcity.loborobot.com.smartcityapp.R;
import smartcity.loborobot.com.smartcityapp.callable.PrivateActivitySchedulersProcedure;
import smartcity.loborobot.com.smartcityapp.callable.Procedure;
import smartcity.loborobot.com.smartcityapp.model.SchedulerModel;

public class SchedulersActivity extends Activity {

    public static class ObjectSingleton
    {
        private static ObjectSingleton singleton;
        private static SchedulersActivity sActivity;
        private static Activity activity;
        public static ObjectSingleton getSingleton(){
            if(singleton==null){
                singleton=new ObjectSingleton();
            }
            return singleton;
        }
        private ObjectSingleton(){

        }
        private static ListView listViewSchedulers;

        public static ListView getListView(){
            return listViewSchedulers;
        }
        public static void setListView(ListView listView){
            listViewSchedulers=listView;
        }

        public static SchedulersActivity getSchedulersActivity(){
            return sActivity;
        }
        public static void setSchedulersActivity(SchedulersActivity sa){
            sActivity=sa;
        }

        public static Activity getActivity(){
            return activity;
        }
        public static void setActivity(Activity a){
            activity=a;
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.extra_activity_schedulers);
        ObjectSingleton.setActivity(this);
        ObjectSingleton.setSchedulersActivity(this);

        LoadList();
    }

    public void LoadList(){
        Procedure procedure=new PrivateActivitySchedulersProcedure();
        SchedulerModel model=new SchedulerModel();
        model.setProcedure(procedure);
        model.setActivity(this);
        model.getPrivateAll();
    }
}