package smartcity.loborobot.com.smartcityapp.extra;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import smartcity.loborobot.com.smartcityapp.MainActivity;
import smartcity.loborobot.com.smartcityapp.R;
import smartcity.loborobot.com.smartcityapp.callable.PrivateSchedulersProcedure;
import smartcity.loborobot.com.smartcityapp.callable.Procedure;
import smartcity.loborobot.com.smartcityapp.extra.data.SchedulerAdapter;
import smartcity.loborobot.com.smartcityapp.extra.data.SchedulerData;
import smartcity.loborobot.com.smartcityapp.model.SchedulerModel;

/**
 * Created by fincyt on 09/01/15.
 */

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */

 public class PlaceholderSchedulersPrivateFragment extends Fragment {

        private LayoutInflater inflater;
        public void setLayoutInflater(LayoutInflater l){
            inflater=l;
        }
        public LayoutInflater getLayoutInflater(){
            return inflater;
        }
        private ViewGroup container;
        public void setViewGroup(ViewGroup v){
            container=v;
        }
        public ViewGroup getViewGroup(){
            return container;
        }
        private Bundle savedInstanceState;

        public void setBundle(Bundle b){
            savedInstanceState=b;
        }
        public Bundle getBundle(){
            return savedInstanceState;
        }

        private ListView listViewSchedulers;

        private static MainPrivateActivity activity;

        public static void setMainPrivateActivity(MainPrivateActivity a){
            activity=a;
        }
        public static MainPrivateActivity getMainPrivateActivity(){
            return activity;
        }

        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderSchedulersPrivateFragment newInstance(int sectionNumber) {
            PlaceholderSchedulersPrivateFragment fragment = new PlaceholderSchedulersPrivateFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderSchedulersPrivateFragment() {

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            setLayoutInflater(inflater);
            setViewGroup(container);
            setBundle(savedInstanceState);

            View rootView = inflater.inflate(R.layout.extra_fragment_schedulers, container, false);

            try {
                listViewSchedulers = (ListView) rootView.findViewById(R.id.gridViewSchedulers);
                /*listViewSchedulers.setOnItemClickListener(
                    new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Message.getSingleton().setContext(MainPrivateActivity.ActivityContainer.getSingleton().getActivity().getApplicationContext());
                            Message.getSingleton().show("Hola este es un tap");
                        }
                    }
                );*/
                MainPrivateActivity.PlaceholderPrivateFragment.ListViewContainer.getSingleton().setListView(listViewSchedulers);
            }catch(Exception ex){}

            try{
                MainPrivateActivity.PlaceholderPrivateFragment.LayoutInflaterContainer.getSingleton().setLayoutInflater(inflater);
            }catch(Exception ex){}
/*
            Procedure procedure=new PrivateSchedulersProcedure();
            SchedulerModel model=new SchedulerModel();
            model.setProcedure(procedure);
            model.setActivity(getActivity());
            model.getAll();*/

            Intent intent = null;
            try{
                intent=new Intent(MainActivity.ActivityContainer.getSingleton().getActivity().getApplicationContext(), SchedulersActivity.class);
            }catch(Exception ex){}
            try {
                if(intent!=null) {
                    MainActivity.ActivityContainer.getSingleton().getActivity().startActivity(intent);
                }
            }catch(Exception ex){}

            /*SchedulerData scheduler_data[] = new SchedulerData[]
                    {
                            new SchedulerData("Scheduler 1",true),
                            new SchedulerData("Scheduler 2",false),
                            new SchedulerData("Scheduler 3",true)
                    };

            SchedulerAdapter adapter=null;
            try {
                adapter = new SchedulerAdapter(getMainPrivateActivity().getBaseContext(), R.layout.extra_schedulers_listview_item_row, scheduler_data);
                if(adapter!=null){
                    adapter.setMainPrivateActivity(getMainPrivateActivity());
                }

            }catch(Exception ex){}

            try {
                listViewSchedulers = (ListView) rootView.findViewById(R.id.gridViewSchedulers);
            }catch(Exception ex){}
            //View header = (View)inflater.inflate(R.layout.extra_schedulers_listview_header_row, null);
            //listViewSchedulers.addHeaderView(header);
            try{
                listViewSchedulers.setAdapter(adapter);
            }catch(Exception ex){}*/

            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainPrivateActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }
