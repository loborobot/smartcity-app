package smartcity.loborobot.com.smartcityapp.http;

/*
*  Codigo desarrollado por Christian Portilla Pauca
*  Mail : xhrist14n@gmail.com
*  *************************************************
*  Proyecto PIMEM 060 2014
*
* */

 public class HttpBaseURL {

    protected static String base_url="http://smartsensor.herokuapp.com/";

	public static String getBaseUrl(){
		return base_url;
	}
}